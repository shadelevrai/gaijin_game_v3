require('dotenv').config()

const express = require("express");
const fs = require("fs").promises;
const bodyParser = require("body-parser");
const cors = require("cors");
const http = require("http");
const socketio = require("socket.io");
const path = require('path');

const app = express();
const httpServer = http.createServer(app);

const io = socketio(httpServer, {
    cors: {},
});

app.use(express.static(__dirname + "/"));
app.use(bodyParser.json());
app.use(cors());

app.get("/game-list", async(req, res) => {
    let dirPath = path.join(__dirname, '../front/public/media')
    const folders = await fs.readdir(dirPath);
    res.json(folders)
})

app.get("/game-number-list/:game", async(req, res) => {
    const { game } = req.params
    // let dirPath = path.join(__dirname, `../front/public/media/${game}`)
    let dirPath = path.join(__dirname, `../front/public/media/${game}`)
    const folders = await fs.readdir(dirPath);
    res.json(folders)
})

app.get("/game/:gameName/:gameNumber", async(req, res) => {
    const { gameName, gameNumber } = req.params
    const dirPath = `../front/public/media/${gameName}/${gameNumber}`;
    const filders = await fs.readdir(dirPath);
    res.json(filders);
});

app.get("/game/json/:gameName/:gameNumber", async (req, res) => {
    const { gameName, gameNumber } = req.params
    const data = require(`../front/public/media/${gameName}/${gameNumber}/${gameName}.js`)
    res.json(data)
});

let test
app.get("/random-media-blindtest", (req,res)=>{
    const dirPath = `../front/public/media/Blind Test`;
    const randomMedia = []
    fs.readdir(dirPath, (err, files) => {
        if (err) return console.log(err);
        files.forEach(element => {
            fs.readdir(`${dirPath}/${element}`,(err,files2)=>{
                files2.forEach(element2 => {
                    randomMedia.push({link:`${element}/${element2}`})
                });
            })
        });
    });
    setTimeout(() => {
        const tableauMelange = randomMedia.sort(() => Math.random() - 0.5);
        const tableauFinal = tableauMelange.slice(0, 25);
        const tableauSansDoublons = [...new Set(tableauFinal)]
        test = tableauSansDoublons
        res.json(tableauSansDoublons);
    }, 2000);
})

app.get("/blindtestchoice", async (req, res) => {
    const dirPath = "../front/public/media/Blind Test";
    const randomMedia = [];

    try {
        // Lire les dossiers dans le répertoire principal
        const folders = await fs.readdir(dirPath);

        // Utiliser Promise.all pour traiter chaque dossier en parallèle
        await Promise.all(folders.map(async (folder) => {
            const folderPath = `${dirPath}/${folder}`;
            const files = await fs.readdir(folderPath);

            // Ajouter chaque fichier trouvé dans randomMedia
            files.forEach(file => {
                randomMedia.push({ link: `${folder}/${file}` });
            });
        }));

        // Mélanger les fichiers, prendre les 25 premiers et enlever les doublons
        //const tableauMelange = randomMedia.sort(() => Math.random() - 0.5);
        
        res.json(randomMedia);
    } catch (err) {
        console.log(err);
        res.status(500).send("Erreur lors de la lecture des fichiers");
    }
});

app.get("/test",(req,res)=>{
    res.json(test)
})

io.on("connection", socket => {
    console.log("socket io connected");
    socket.removeAllListeners(); // Empêche l'accumulation d'événements
    socket.on("gameNumber", (gameChoosen, gameNumber) => {
        socket.broadcast.emit("gameNumber", gameChoosen, gameNumber)
    })
    socket.on("change-media", (numberMedia, mediaLinksLength, gameNumber, gameChoosen) => {
        console.log("Le bouton NEXTMEDIA a été reçu par le serveur")
        /*if(process.argv[2] === 'production'){
            if (numberMedia / mediaLinksLength > 0.5) {
                fs.readFile("./db/game-done.json", (err, data) => {
                    if (err) return console.log("no")
                    const dataParse = JSON.parse(data)
                    const gamePresence = dataParse.find(item => item.gameNumber === gameNumber)
                    if (!gamePresence) {
                        console.log("faire un nouvel element et le mettre dans le json")
                        dataParse.push({ gameName: gameChoosen, gameNumber, state: "start", date: new Date().toUTCString() })
                        fs.writeFile("./db/game-done.json", JSON.stringify(dataParse), err => {
                            if (err) return console.log("error")
                            console.log("ajout effectué")
                        })
                    } else {
                        const position = dataParse.map(item => item.gameNumber).indexOf(gameNumber)
                        dataParse[position].state = numberMedia
                        dataParse[position].date = new Date().toUTCString()
                        fs.writeFile("./db/game-done.json", JSON.stringify(dataParse), err => {
                            if (err) return console.log("error")
                            console.log("maj effectué")
                        })
                    }
                })
            }
        }*/
        socket.broadcast.emit("change-media", numberMedia)
        console.log("Le bouton NEXTMEDIA a été envoyé par le serveur")
    })
    socket.on("show-response", () => {
        console.log("Le bouton SHOWREPONSE a été reçu par le serveur")
        socket.broadcast.emit("show-response")
        console.log("Le bouton SHOWREPONSE a été envoyé par le serveur")
    })
    socket.on("play", () => {
        console.log("Le bouton PLAY a été reçu par le serveur")
        socket.broadcast.emit("play")
        console.log("Le bouton PLAY a été énviyé par le serveur")
    })
    socket.on("pause", () => {
        console.log("Le bouton PAUSE a été reçu par le serveur")
        socket.broadcast.emit("pause")
        console.log("Le bouton PAUSE a été envoyé par le serveur")
    })
    socket.on("launchGame", () => {
        socket.broadcast.emit("launchGame")
    })
    socket.on("return-menu", () => {
        console.log("Le bouton RETURNMENU a été reçu par le serveur")
        socket.broadcast.emit("return-menu")
        console.log("Le bouton RETURNMENU a été envoyé par le serveur")
    })
    socket.on("karaoke-playlist", (mediaChoice) => {
        socket.broadcast.emit("karaoke-playlist", mediaChoice)
    })
    socket.on("volume-up", () => {
        socket.broadcast.emit("volume-up")
    })
    socket.on("volume-down", () => {
        socket.broadcast.emit("volume-down")
    })
    socket.on("less-Blur-2", () => {
        socket.broadcast.emit("less-Blur-2")
    })
    socket.on("less-Blur-1", () => {
        socket.broadcast.emit("less-Blur-1")
    })
    socket.on("message_for_video", (message) => {
        socket.broadcast.emit("message_for_video", message)
    })
    socket.on("next-indice", () => {
        socket.broadcast.emit("next-indice")
    })
    socket.on("note-game", (gameNumber, gameChoosen) => {
        console.log("🚀 ~ file: app.js:104 ~ socket.on ~ message:", gameNumber, gameChoosen)
        fs.readFile("./db/game-done.json", (err, data) => {
            if (err) return console.log("no")
            const dataParse = JSON.parse(data)
            const gamePresence = dataParse.find(item => item.jeux === gameNumber)
            if (!gamePresence) {
                console.log("faire un nouvel element et le mettre dans le json")
                dataParse.push({ gameName: gameChoosen, gameNumber, state: "begin" })
                fs.writeFile("./db/game-done.json", JSON.stringify(dataParse), err => {
                    if (err) return console.log("error")
                    console.log("yes")
                })
            } else {
                console.log("upload de l'element")
            }
            // dataParse.push({jeux:message})
            // console.log('dataParse:', dataParse)
        })

    })
    socket.on("next-media-blind-test-choice",(mediaChoice)=>{
        socket.broadcast.emit("next-media-blind-test-choice", mediaChoice)
    })
    socket.on("blind-test-choice-over",()=>{
        socket.broadcast.emit("blind-test-choice-over")
    })
    socket.on("disconnect", () => {
        console.log("socket io disconnected");
    });
})

httpServer.listen(8888, () => {
    console.log("Server listening");
});