### NODE 16.12.0 (voir avec nvm) 

## ETAPE 1

S'assurer qu'on est bien en version node 16.12.0 (faire nvm list)
S'assurer que avec Firefox, les lectures automatiques sont activées
Si la commande nvm ne fonctionne pas, il faut installer nvm

## ETAPE 2

Faire un npm install en ligne de commande dans le dossier FRONT et et le dossier BACK
Dans le dossier Front, Faire npm install -g concurrently si concurrently n'est pas installé

## ETAPE 3 

Récupérez le dossier média (qui doit être dans le onedrive de shade) et le mettre dans le dossier front/public

## ETAPE 4

Connecter le telephone et le pc à la box et via le pc (adresse http://192.168.0.1/ pour la box RED SFR), vérifier que le port 3000 est ouvert (normalement c'est déja fait)

## ETAPE 5
Trouver l'adresse IP du pc où le serveur est lancé via la ligne de commande ipconfig et chercher la ligne Adresse IPv4

## ETAPE 6

Mettre l'adresse l'IP locale en variable d'environnement (front/.env)

## ETAPE 

Dans le dossier FRONT, faire en ligne de commande 'npm run build' pour build le projet et faire 'npm run start' pour le lancer



## ça ne fonctionne pas, que dois-je faire ?

Le mieux est de relancer l'application. Pour se faire, placez-vous dans la console (en dessous) et faites plusieurs fois CTRL + C
Ensuite, appuyez sur la fleche du haut du clavier et appuyez sur ENTREE


