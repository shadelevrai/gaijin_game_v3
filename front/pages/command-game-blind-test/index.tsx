import { NextPage } from "next";
import { useEffect, useState } from "react";
import { useRouter } from "next/router"
import { getGameMediaAPI, getRandomMediaBlindTest } from "../../libs/API";
import socketIOClient from "socket.io-client";
import ip from "ip";
let socket: any; // Déclaration globale

if (!socket) {
    socket = socketIOClient(`http://${process.env.NEXT_PUBLIC_REACT_APP_CLIENT_IP}:8888`);
    console.log("Socket.IO connecté :", socket.id);
}
console.log("ip ", ip.address())
{/* @ts-ignore: Unreachable code error */ }
const CommandGameBlindTest: NextPage = ({ REACT_APP_CLIENT_IP }) => {

    const router = useRouter()
    const { gameNumber, gameChoosen } = router.query

    const [mediaLinks, getMediaLinks] = useState<[]>([])
    const [numberMedia, setNumberMedia] = useState<number>(0)
    const [launchGame, setLaunchGame] = useState<boolean>(false)


    useEffect(() => {
        getGame()
        // socket.emit("note-game", gameNumber, gameChoosen)
    }, [])

    useEffect(() => {
        socket.emit("change-media", numberMedia, mediaLinks.length, gameNumber, gameChoosen)
    }, [numberMedia])

    async function getGame() {
        gameNumber === "blind test 00 RANDOM" ?
            mediaRandom()
            :
            {/* @ts-ignore: Unreachable code error */ }
            ;
        {/* @ts-ignore: Unreachable code error */ }
        getMediaLinks(await getGameMediaAPI("Blind Test", gameNumber, REACT_APP_CLIENT_IP));
    }

    function nextMedia() {
        setNumberMedia(numberMedia + 1)
        console.log("Le bouton NEXTMEDIA a été pressé")
    }

    function backMedia() {
        setNumberMedia(numberMedia - 1)

    }

    function showResponse() {
        socket.emit("show-response")
        console.log("Le bouton SHOWREPONSE a été pressé")
    }

    function pause() {
        socket.emit("pause")
        console.log("Le bouton PAUSE a été pressé")
    }

    function play() {
        socket.emit("play")
        console.log("Le bouton PLAY a été pressé")
    }

    function launchGameFunc() {
        setLaunchGame(true)
        socket.emit("launchGame")
    }

    function menu() {
        socket.emit("return-menu")
        console.log("Le bouton RETOURMENU a été pressé")
        router.push({
            pathname: "/"
        })
    }

    function sliceRandomMediaNext() {
        // @ts-ignore: Unreachable code error 
        const texte = mediaLinks[numberMedia + 1].link
        let position = texte.indexOf("/");
        let deuxLettres = texte.slice(position + 3, -4);
        return deuxLettres
    }

    function sliceRandomMedia() {
        // @ts-ignore: Unreachable code error 
        const texte = mediaLinks[numberMedia].link
        let position = texte.indexOf("/");
        let deuxLettres = texte.slice(position + 3, -4);
        return deuxLettres
    }

    function showNextResponse() {
        let result
        try {
            {/* @ts-ignore: Unreachable code error */ }
            gameNumber === "blind test 00 RANDOM" ?
                result = <p>Réponse suivant :
                    {/* @ts-ignore: Unreachable code error */}
                    {mediaLinks.length && sliceRandomMediaNext()}</p>
                :
                result = <p>Réponse suivant :
                    {/* @ts-ignore: Unreachable code error */}
                    {mediaLinks.length && mediaLinks[numberMedia + 1].slice(3, -4)}</p>

        } catch (error) {
            return <p>C'est la dernière</p>
        }
        return result
    }

    async function mediaRandom() {
        const mediaRandom = await getRandomMediaBlindTest(REACT_APP_CLIENT_IP)
        console.log("🚀 ~ file: index.tsx:81 ~ mediaRandom ~ mediaRandom:", mediaRandom)
        getMediaLinks(mediaRandom)

    }
    return (
        <div>
            <>
                {!launchGame ?
                    <><button className="button" onClick={() => launchGameFunc()}> Lancer le jeu </button> <button className="button" onClick={() => menu()}>retour au menu</button> </> :
                    <>
                        {numberMedia + 1 > mediaLinks.length ?
                            <>
                                <p>le jeu est terminé</p>
                                <button className="button" onClick={() => menu()}>retour au menu</button>
                            </>
                            :
                            <>
                                {/* @ts-ignore: Unreachable code error */}
                                {gameNumber === "blind test 00 RANDOM" ?
                                    <p>Réponse :
                                        {/* @ts-ignore: Unreachable code error */}
                                        {mediaLinks.length && sliceRandomMedia()} </p>
                                    :
                                    <> {console.log(mediaLinks)}
                                        <p>Réponse :
                                            {/* @ts-ignore: Unreachable code error */}
                                            {mediaLinks.length && mediaLinks[numberMedia].slice(3, -4)}  </p>
                                    </>
                                }
                                {showNextResponse()}
                                {numberMedia != 0 && <button className="button" onClick={() => backMedia()}>Média précédent</button>}
                                <button className="button is-medium" onClick={() => pause()}>Pause</button>
                                <button className="button is-medium" onClick={() => play()}>play</button>
                                <button className="button is-medium" onClick={() => nextMedia()}>Média suivant</button>
                                <button className="button is-medium" onClick={() => showResponse()}>Réponse</button>
                                <button className="button is-medium" onClick={() => menu()}>retour au menu</button>
                                <button className="button is-medium" onClick={() => socket.emit("volume-up")}>volume +</button>
                                <button className="button is-medium" onClick={() => socket.emit("volume-down")}>Volume - </button>
                            </>
                        }
                    </>
                }
            </>
        </div>
    )
}

export async function getServerSideProps() {
    const REACT_APP_CLIENT_IP = process.env.REACT_APP_CLIENT_IP
    return {
        props: {
            REACT_APP_CLIENT_IP
        }
    }
}
export default CommandGameBlindTest