import { NextPage } from "next";
import socketIOClient from "socket.io-client";
import { useRouter } from "next/router"
import { getGameMediaAPI } from "../../libs/API";
import { useState, useEffect } from "react";
import ip from "ip";
let socket: any; // Déclaration globale

if (!socket) {
    socket = socketIOClient(`http://${process.env.NEXT_PUBLIC_REACT_APP_CLIENT_IP}:8888`);
    console.log("Socket.IO connecté :", socket.id);
}

{/* @ts-ignore */ }
const CommandGameDezoom: NextPage = ({ REACT_APP_CLIENT_IP }) => {
    const router = useRouter()
    const { gameNumber, gameChoosen } = router.query

    const [mediaLinks, getMediaLinks] = useState<[]>([])
    const [launchGame, setLaunchGame] = useState<boolean>(false)
    const [numberMedia, setNumberMedia] = useState<number>(0)

    useEffect(() => {
        getGame()
    }, [])

    useEffect(() => {
        socket.emit("change-media", numberMedia, mediaLinks.length, gameNumber, gameChoosen)
    }, [numberMedia])

    async function getGame() {
        {/* @ts-ignore: Unreachable code error */ }
        const mediaLinks = await getGameMediaAPI("Dezoom", gameNumber, REACT_APP_CLIENT_IP);
        getMediaLinks(mediaLinks);
    }

    function menu() {
        socket.emit("return-menu")
        router.push({
            pathname: "/"
        })
    }

    function launchGameFunc() {
        setLaunchGame(true)
        socket.emit("launchGame")
    }

    function lessBlur2() {
        socket.emit("less-Blur-2")
    }
    function lessBlur1() {
        socket.emit("less-Blur-1")
    }
    function showResponse() {
        socket.emit("show-response")
    }

    function nextMedia() {
        setNumberMedia(numberMedia + 1)
    }

    function backMedia() {
        setNumberMedia(numberMedia - 1)
    }

    function showNextResponse() {
        try {
            {/* @ts-ignore: Unreachable code error */ }
            return <p>Réponse suivant : {mediaLinks.length && mediaLinks[numberMedia + 1].slice(3, -4)}</p>
        } catch (error) {
            return <p>C'est la dernière</p>
        }
    }


    return (<div>
        {!launchGame && <>
            <button className="button" onClick={() => launchGameFunc()}> Lancer le jeu </button>
            <button className="button" onClick={() => menu()}>retour au menu</button>
        </>}
        {launchGame && <>
            {/* @ts-ignore: Unreachable code error */}
            <p>Réponse : {mediaLinks.length && mediaLinks[numberMedia].slice(3, -4)} </p>
            {showNextResponse()}
            {numberMedia != 0 && <button className="button" onClick={() => backMedia()}>Média précédent</button>}
            <button className="button" onClick={() => lessBlur2()}>Baisser le flou par 2</button>
            <button className="button" onClick={() => lessBlur1()}>Baisser le flou par 1</button>
            <button className="button" onClick={() => nextMedia()}>Média suivant</button>
            <button className="button" onClick={() => showResponse()}>Réponse</button>
            <button className="button" onClick={() => menu()}>retour au menu</button>
        </>}
    </div>)
}

export async function getServerSideProps() {
    const REACT_APP_CLIENT_IP = process.env.REACT_APP_CLIENT_IP
    return {
        props: {
            REACT_APP_CLIENT_IP
        }
    }
}

export default CommandGameDezoom