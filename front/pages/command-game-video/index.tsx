import { NextPage } from "next";
import { useRouter } from "next/router"
import { useEffect, useState } from "react";
import { getGameMediaAPI } from "../../libs/API";
import socketIOClient from "socket.io-client";

import ip from "ip";
let socket: any; // Déclaration globale

if (!socket) {
    socket = socketIOClient(`http://${process.env.NEXT_PUBLIC_REACT_APP_CLIENT_IP}:8888`);
    console.log("Socket.IO connecté :", socket.id);
}

{/* @ts-ignore: Unreachable code error */}
const CommandGameVideo: NextPage = ({ REACT_APP_CLIENT_IP }) => {

    const router = useRouter()
    {/* @ts-ignore: Unreachable code error */}
    const gameNumber: string = router.query.gameNumber

    const [mediaLinks, getMediaLinks] = useState<[string] | []>([])
    const [mediaChoice, setMediaChoice] = useState<[string] | []>([])
    const [videoBegin, setVideoBegin] = useState<boolean>(false)
    const [message, setMessage] = useState<string>("")

    useEffect(() => {
        getGame()
    }, [])

    function removeFromMEdiaChoosen(media: string): void {
        const mediasChoosenWithRemove = mediaChoice.filter((value) => value != media)
        {/* @ts-ignore: Unreachable code error */}
        setMediaChoice(mediasChoosenWithRemove)
    }

    async function getGame(): Promise<void> {
        const mediaLinks = await getGameMediaAPI("Video", gameNumber,REACT_APP_CLIENT_IP);
        getMediaLinks(mediaLinks);
    }

    function validate() {
        socket.emit("karaoke-playlist", mediaChoice)
        setVideoBegin(true)
    }

    function menu() {
        socket.emit("return-menu")
        router.push({
            pathname: "/"
        })
    }

    {/* @ts-ignore: Unreachable code error */}
    function chooseMediaFunc(media) {
        {/* @ts-ignore: Unreachable code error */}
        setMediaChoice(mediaChoice => [...mediaChoice, media])
    }

    function messageFunc(message:string) {
        setMessage(message)
    }

    function validateMessage(){
        socket.emit("message_for_video", message)
    }

    function shuffle() {
        const shuffledArray = mediaChoice.sort((a, b) => 0.5 - Math.random());
        setMediaChoice(shuffledArray)
    }

    return (
        <div>
            {mediaLinks.map(media => <button className="button" key={media} onClick={() => chooseMediaFunc(media)} disabled={videoBegin ? true : false}> {media} </button>)}
            <h1 className="title">La liste</h1>
            {mediaChoice.map(media => <button className="button"  key={media} onClick={() => removeFromMEdiaChoosen(media)} disabled={videoBegin ? true : false}> {media} </button>)}
            <button className="button" onClick={() => validate()} disabled={mediaChoice.length > 0 ? false : true}>Valider la playlist</button>
            {mediaChoice.length*1.5}
            <button disabled={mediaChoice.length > 0 ? false : true} onClick={()=>shuffle()} className="button">Mélanger la playlist choisie</button>
            <button className="button" onClick={() => socket.emit("volume-up")}>volume +</button>
            <button className="button" onClick={() => socket.emit("volume-down")}>Volume - </button>
            <button className="button" onClick={() => menu()}>retour au menu</button>
            <p>Mettre un message</p>
            <input onChange={(e) => messageFunc(e.target.value)} />
            <button className="button" onClick={()=>validateMessage() }>valdier le message</button>
        </div>
    )
}

export async function getServerSideProps() {
    const REACT_APP_CLIENT_IP = process.env.REACT_APP_CLIENT_IP
    return {
        props: {
            REACT_APP_CLIENT_IP
        }
    }
}

export default CommandGameVideo