import { NextPage } from "next";
import { useEffect, useState } from "react";
import { useRouter } from "next/router"
import { getGameMediaAPIBlindTestChoice } from "../../libs/API";
import socketIOClient from "socket.io-client";
import ip from "ip";
let socket: any; // Déclaration globale

if (!socket) {
    socket = socketIOClient(`http://${process.env.NEXT_PUBLIC_REACT_APP_CLIENT_IP}:8888`);
    console.log("Socket.IO connecté :", socket.id);
}

{/* @ts-ignore: Unreachable code error */ }
const CommandGameBlindTest: NextPage = ({ REACT_APP_CLIENT_IP }) => {

    const router = useRouter()

    const [mediaLinks, getMediaLinks] = useState<[]>([])
    const [numberMedia, setNumberMedia] = useState<number>(0)
    const [launchGame, setLaunchGame] = useState<boolean>(false)
    const [mediasChoices, setMediasChoices] = useState<any>([])
    const [mediaChoice, setMediaChoice] = useState<string>("")


    useEffect(() => {
        getGame()
        // socket.emit("note-game", gameNumber, gameChoosen)
    }, [])


    useEffect(() => {
        if (mediaLinks.length > 0) {
            const tableauMelange = mediaLinks.sort(() => Math.random() - 0.5);
            const tableauFinal = tableauMelange.slice(0, 25);
            //console.log(tableauFinal)
            setMediasChoices(tableauFinal)
        }
    }, [mediaLinks])

    async function getGame() {
        {/* @ts-ignore: Unreachable code error */ }
        const allMedias = await getGameMediaAPIBlindTestChoice(REACT_APP_CLIENT_IP)
        getMediaLinks(allMedias);
    }

    function menu() {
        socket.emit("return-menu")
        router.push({
            pathname: "/"
        })
    }

    function launchGameFunc() {
        setLaunchGame(true)
        socket.emit("launchGame")
    }

    function setMediaChoiceFunc(nextmedia: string) {
        setMediaChoice(nextmedia)
    }

    function pause() {
        socket.emit("pause")
    }

    function play() {
        socket.emit("play")
    }
    
    function showResponse() {
        socket.emit("show-response")
    }

    function formatResponse(){
        const num = mediaChoice.indexOf("/");
		console.log("TCL: formatResponse -> num", num)
        return mediaChoice.substring(num+4,mediaChoice.length-4);
    }

    useEffect(() => {
        mediaChoice && socket.emit("next-media-blind-test-choice", mediaChoice);
        getGame()
    }, [mediaChoice])

    return (
        <div>
            <>
                {!launchGame && <>
                    <button className="button" onClick={() => launchGameFunc()}> Lancer le jeu </button>
                    <button className="button" onClick={() => menu()}>retour au menu</button>
                </>}
                {launchGame && <>
                    <p>Réponse : {formatResponse()}</p>
                    <p>Choisissez le prochain média</p>
                    {mediasChoices.map((medias: any) => (<button className="button is-medium has-background-primary" onClick={() => setMediaChoiceFunc(medias.link)}>{medias.link}</button>))}
                    <button className="button is-medium" onClick={() => pause()}>Pause</button>
                    <button className="button is-medium" onClick={() => play()}>play</button>
                    <button className="button is-medium" onClick={() => showResponse()}>Réponse</button>
                    <button className="button is-medium" onClick={() => socket.emit("volume-up")}>volume +</button>
                    <button className="button is-medium" onClick={() => socket.emit("volume-down")}>Volume - </button>
                    <button className="button is-medium" onClick={() => socket.emit("blind-test-choice-over")}>Fin du blind test</button>
                    <button className="button" onClick={() => menu()}>retour au menu</button>
                </>}
            </>
        </div>
    )
}

export async function getServerSideProps() {
    const REACT_APP_CLIENT_IP = process.env.REACT_APP_CLIENT_IP
    return {
        props: {
            REACT_APP_CLIENT_IP
        }
    }
}
export default CommandGameBlindTest