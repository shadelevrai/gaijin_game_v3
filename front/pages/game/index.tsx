import { useEffect, useState } from "react";
import socketIOClient from "socket.io-client";
import BlindTest from "../../components/BlindTest";
import Video from "../../components/Video";
import ReseauxSociauxCadre from "../../components/ReseauxSociauxCadre";
import BlurryGame from "../../components/BlurryGame";
import JDM from "../../components/JDM";
import QCM from "../../components/QCM";
import VOF from "../../components/VOF";
import JDI from "../../components/JDI";
import Dezoom from "../../components/Dezoom";
import Quizz from "../../components/Quizz"
import Notanime from "../../components/Notanime"
import BlindTestChoice from "../../components/BlindTestChoice";

const socket = socketIOClient("http://localhost:8888");

const Game = () => {

    interface Game {
        gameName: string,
        gameNumber: string
    }

    const [game, setGame] = useState<Game>({
        gameName: "",
        gameNumber: ""
    })

    useEffect(() => {
        socket.on("gameNumber", (gameChoosen, gameNumber) => {
            setGame(() => ({
                gameName: gameChoosen,
                gameNumber: gameNumber
            }))
        })
    }, [])

    return (
        <div>
            {game.gameName === "" && <>
                <img src="../img/logo.png" style={{ width: "50vw", marginLeft: "25vw",marginTop:"5vh" }} />
                <p className="descriptionGaijinTeam">L'association qui a<span style={{opacity:"0.2"}}>n</span>ime le Japon</p>
                <div style={{ marginLeft: "25vw",marginTop:"1vh" }}>
                    <ReseauxSociauxCadre cadre={false} />
                </div>
            </>}
            {game.gameName === "Blind Test Choice" &&<BlindTestChoice/>}
            {game.gameName === "Blind Test" && <BlindTest gameNumber={game.gameNumber} />}
            {/* @ts-ignore: Unreachable code error */}
            {game.gameName === "Video" && <Video gameName={game.gameName} gameNumber={game.gameNumber} /> }
            {game.gameName === "Blurry Game" && <BlurryGame gameNumber={game.gameNumber} /> }
            {game.gameName === "JDM" && <JDM gameNumber={game.gameNumber} /> }
            {game.gameName === "QCM" && <QCM gameNumber={game.gameNumber} /> }
            {game.gameName === "VOF" && <VOF gameNumber={game.gameNumber} /> }
            {game.gameName === "JDI" && <JDI gameNumber={game.gameNumber} /> }
            {game.gameName === "Dezoom" && <Dezoom gameNumber={game.gameNumber} /> }
            {game.gameName === "Quizz" && <Quizz gameNumber={game.gameNumber} /> }
            {game.gameName === "Notanime" && <Notanime gameNumber={game.gameNumber} /> }
        </div>
    )
}

export default Game