import { NextPage } from "next";
import { useRouter } from "next/router"
import { useEffect, useState } from "react";
import { getGameJSONAPI } from "../../libs/API";
import socketIOClient from "socket.io-client";
import ip from "ip";
let socket: any; // Déclaration globale

if (!socket) {
    socket = socketIOClient(`http://${process.env.NEXT_PUBLIC_REACT_APP_CLIENT_IP}:8888`);
    console.log("Socket.IO connecté :", socket.id);
}

{/* @ts-ignore: Unreachable code error */}
const commandGameJDM: NextPage = ({ REACT_APP_CLIENT_IP }:any) => {

    const router = useRouter()
    {/* @ts-ignore: Unreachable code error */}
    const { gameNumber,gameChoosen } = router.query

    const [words, setWords] = useState<[string] | []>([])
    const [numberMedia, setNumberMedia] = useState<number>(0)
    const [launchGame, setLaunchGame] = useState<boolean>(false)

    useEffect(() => {
        getGame()
    }, [])

    useEffect(() => {
        socket.emit("change-media", numberMedia,words.length, gameNumber, gameChoosen)
    }, [numberMedia])

    async function getGame(): Promise<void> {
        {/* @ts-ignore */}
        const mediaLinks = await getGameJSONAPI("JDM", gameNumber, REACT_APP_CLIENT_IP);
        setWords(mediaLinks);
    }

    function nextMedia(): void {
        setNumberMedia(numberMedia + 1)
    }

    function backMedia(): void {
        setNumberMedia(numberMedia - 1)
    }

    function launchGameFunc(): void {
        setLaunchGame(true)
        socket.emit("launchGame")
    }

    function showResponse(): void {
        socket.emit("show-response")
    }

    function menu(): void {
        socket.emit("return-menu")
        router.push({
            pathname: "/"
        })
    }

    function nextIndice(): void {
        socket.emit("next-indice")
    }

    return (
        <div>
            {!launchGame ? <>
                <button className="button" onClick={() => launchGameFunc()}> Lancer le jeu </button> <button className="button" onClick={() => menu()}>retour au menu</button>
            </> : <>
            {/* @ts-ignore: Unreachable code error */}
            { numberMedia < words.length ? <p>Reponse : {words[numberMedia].words[0]} {numberMedia === words.length-1 && <>- c'est le dernier</>} </p>:<p>c'est fini</p>}
                <button className="button" onClick={() => nextIndice()}>Indice suivant</button>
                {numberMedia != 0 && <button className="button" onClick={() => backMedia()}>Média précédent</button>}
                <button className="button" onClick={() => nextMedia()}>Média suivant</button>
                <button className="button" onClick={() => showResponse()}>Réponse</button>
                <button className="button" onClick={() => menu()}>retour au menu</button>
            </>}
        </div>
    )

}

export async function getServerSideProps() {
    const REACT_APP_CLIENT_IP = process.env.REACT_APP_CLIENT_IP
    return {
        props: {
            REACT_APP_CLIENT_IP
        }
    }
}

export default commandGameJDM