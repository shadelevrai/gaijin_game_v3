import { NextPage } from "next";
import socketIOClient from "socket.io-client";
import { useRouter } from "next/router"
import { useEffect, useState } from "react";
import { getGameMediaAPI } from "../../libs/API";
import ip from "ip";
let socket: any; // Déclaration globale

if (!socket) {
    socket = socketIOClient(`http://${process.env.NEXT_PUBLIC_REACT_APP_CLIENT_IP}:8888`);
    console.log("Socket.IO connecté :", socket.id);
}

{/* @ts-ignore */}
const CommandGameJDI: NextPage = ({ REACT_APP_CLIENT_IP }) => {

    const [mediaLinks, getMediaLinks] = useState<[]>([])
    const [numberMedia, setNumberMedia] = useState<number>(0)
    const [launchGame, setLaunchGame] = useState<boolean>(false)
    const [gameVersionEyes,setGameVersionEyes] = useState<boolean>(false)

    const router = useRouter()
    const { gameNumber,gameChoosen } = router.query

    useEffect(() => {
        getGame()
    }, [])

    useEffect(() => {
        socket.emit("change-media", numberMedia,mediaLinks.length, gameNumber, gameChoosen)
    }, [numberMedia])

    function launchGameFunc(): void {
        setLaunchGame(true)
        socket.emit("launchGame")
    }

    function menu(): void {
        socket.emit("return-menu")
        router.push({
            pathname: "/"
        })
    }

    function nextMedia() {
        setNumberMedia(numberMedia + 1)
    }

    async function getGame() {
        {/* @ts-ignore: Unreachable code error */ }
        const mediaLinks = await getGameMediaAPI("JDI", gameNumber, REACT_APP_CLIENT_IP);
        getMediaLinks(mediaLinks);
        mediaLinks[0].slice(3,9) === "[EYES]" && setGameVersionEyes(true)
    }

    function showResponse() {
        socket.emit("show-response")
    }

    function backMedia() {
        setNumberMedia(numberMedia - 1)
    }

    return (<div>
        {!launchGame && <>
            <button className="button" onClick={() => launchGameFunc()}> Lancer le jeu </button>
        </>}
        {launchGame && <>
        {console.log(mediaLinks)}
        {/* @ts-ignore: Unreachable code error */}
            <button className="button" onClick={() => nextMedia()}>Média suivant</button>
            {numberMedia != 0 && <button className="button" onClick={() => backMedia()}>Média précédent</button>}
             {!gameVersionEyes && <button className="button" onClick={() => showResponse()}>Réponse</button>} 
        </>}

        <button className="button" onClick={() => menu()}>retour au menu</button>
    </div>)
}
export async function getServerSideProps() {
    const REACT_APP_CLIENT_IP = process.env.REACT_APP_CLIENT_IP
    return {
        props: {
            REACT_APP_CLIENT_IP
        }
    }
}
export default CommandGameJDI