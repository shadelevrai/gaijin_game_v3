import { NextPage } from "next"
import socketIOClient from "socket.io-client";
import { useEffect, useState } from "react";
import { useRouter } from "next/router"
import { getGameMediaAPI } from "../../libs/API";
import ip from "ip";
let socket: any; // Déclaration globale

if (!socket) {
    socket = socketIOClient(`http://${process.env.NEXT_PUBLIC_REACT_APP_CLIENT_IP}:8888`);
    console.log("Socket.IO connecté :", socket.id);
}

{/* @ts-ignore */}
const CommandGameVOF: NextPage = ({ REACT_APP_CLIENT_IP }) => {

    const router = useRouter()
    const { gameNumber,gameChoosen } = router.query

    const [mediaLinks, getMediaLinks] = useState<[]>([])
    const [numberMedia, setNumberMedia] = useState<number>(0)
    const [launchGame, setLaunchGame] = useState<boolean>(false)

    useEffect(() => {
        getGame()
    }, [])

    useEffect(() => {
        socket.emit("change-media", numberMedia,mediaLinks.length, gameNumber, gameChoosen)
    }, [numberMedia])

    function menu(): void {
        socket.emit("return-menu")
        router.push({
            pathname: "/"
        })
    }

    function launchGameFunc():void {
        setLaunchGame(true)
        socket.emit("launchGame")
    }

    async function getGame(): Promise<void> {
        {/* @ts-ignore: Unreachable code error */ }
        const mediaLinks = await getGameMediaAPI("VOF", gameNumber, REACT_APP_CLIENT_IP);
        getMediaLinks(mediaLinks);
    }

    function nextMedia() {
        setNumberMedia(numberMedia + 1)
    }

    function showResponse() {
        socket.emit("show-response")
    }

    function backMedia() {
        setNumberMedia(numberMedia - 1)
    }


    return (<>
        {!launchGame ? 
        <button className="button" onClick={() => launchGameFunc()}> Lancer le jeu </button>
         : 
         <button className="button" onClick={() => nextMedia()}>Média suivant</button>}
         <button className="button" onClick={() => showResponse()}>Réponse</button>
         {numberMedia != 0 && <button className="button" onClick={() => backMedia()}>Média précédent</button>}
        <button className="button" onClick={() => menu()}>retour au menu</button>
    </>)

}

export async function getServerSideProps() {
    const REACT_APP_CLIENT_IP = process.env.REACT_APP_CLIENT_IP
    return {
        props: {
            REACT_APP_CLIENT_IP
        }
    }
}
export default CommandGameVOF