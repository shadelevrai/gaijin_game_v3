import type { AppProps } from 'next/app'
import "bulma"
import "../styles/globals.scss"
import "../styles/ReseauxSociaux.scss"
import "../styles/blindTest.scss"
import "../styles/blurryGame.scss"
import "../styles/jdm.scss"
import "../styles/qcm.scss"
import "../styles/video.scss"
import "../styles/vof.scss"
import "../styles/jdi.scss"
import "../styles/quizz.scss"
import "../styles/notanime.scss"
import { useState, useEffect } from 'react';


function MyApp({ Component, pageProps }: AppProps) {

  // const [windowSizeWidth, setWindowSizeWidth] = useState(() => {
  //   try {
  //     return window.innerWidth
  //   } catch (error) {
  //     return null
  //   }
  // });
  // const [windowSizeHeight, setWindowSizeHeight] = useState(() => {
  //   try {
  //     return window.innerHeight
  //   } catch (error) {
  //     return null
  //   }
  // });

  // useEffect(() => {
  //   const handleSize = function () {
  //     window && (setWindowSizeWidth(window.innerWidth), setWindowSizeHeight(window.innerHeight));
  //   };

  //   window.addEventListener("resize", handleSize);
  // }, [])

  // useEffect(() => {
  //   console.log("🚀 ~ file: _app.tsx ~ line 12 ~ MyApp ~ windowSizeWidth", windowSizeWidth)
  //   console.log("🚀 ~ file: _app.tsx ~ line 13 ~ MyApp ~ windowSizeHeight", windowSizeHeight)
  // })

  return (
    // <div className='zone1080'>
    // <div className='zone720'>
    <Component {...pageProps} />
    // </div>
    // </div>
  )
}

export default MyApp
