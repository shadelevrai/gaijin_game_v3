import { useEffect, useState } from "react"

const Test = () => {

    const [data,setData]=useState(0)

    useEffect(()=>{
        //se lance au début
    },[])

    useEffect(()=>{
        //se lance à chaque modification
    })

    useEffect(()=>{
        //se lance quand la variable data se change
    },[data])
    return (
        <div>
            <p onClick={()=>console.log("lala")}></p>
        </div>
    )
}

export default Test