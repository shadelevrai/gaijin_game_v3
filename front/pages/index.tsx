import type { NextPage } from "next"
import { useEffect, useState } from "react"
import { useRouter } from 'next/router'
import socketIOClient from "socket.io-client";
import memory from "../../back/db/game-done.json"
import ip from "ip";

let socket: any; // Déclaration globale

if (!socket) {
    socket = socketIOClient(`http://${process.env.NEXT_PUBLIC_REACT_APP_CLIENT_IP}:8888`);
    console.log("Socket.IO connecté :", socket.id);
}

{/* @ts-ignore: Unreachable code error */ }
const Home: NextPage = ({ REACT_APP_CLIENT_IP }) => {
    //const socket = socketIOClient(`http://${REACT_APP_CLIENT_IP}:8888`);
    const router = useRouter()

    const [chooseGame, setChooseGame] = useState<[]>([])
    const [gameChoosen, setGameChoosen] = useState<string>("")
    const [chooseGameNumber, setChooseGameNumber] = useState<[]>([])
    const [messageError, setMessageError] = useState<String>("")

    useEffect(() => {
        getGameList()
        // socket.on("launch-game", mediaLinks => {})
    }, [])

    async function getGameList(): Promise<void> {
        try {
            const resRaw = await fetch(`http://${REACT_APP_CLIENT_IP}:8888/game-list`)
            const resData = resRaw.status === 200 ? await resRaw.json() : (setMessageError("Le serveur n'a pas envoyé de status 200"),console.log(resRaw))
            setChooseGame(resData)
        } catch (error: any) {
            setMessageError(error.message)
        }
    }

    async function getGameNumberList(game: string): Promise<void> {
        try {
            setGameChoosen(game)
            const resRaw = await fetch(`http://${REACT_APP_CLIENT_IP}:8888/game-number-list/${game}`)
            const resData = resRaw.status === 200 ? await resRaw.json() : setMessageError("Le serveur n'a pas envoyé de status 200")
            setChooseGameNumber(resData)
        } catch (error: any) {
            setMessageError(error.message)
        }
    }
    
    function gameListChoosen(gameNumber: string): void {
        socket.emit("gameNumber", gameChoosen, gameNumber)
        gameChoosen === "Blind Test Choice" && router.push({
            pathname:"command-game-blind-test-choice"
        })
        gameChoosen === "Blind Test" && router.push({
            pathname: "command-game-blind-test",
            query: {
                gameNumber,
                gameChoosen
            }
        })
        gameChoosen === "Video" && router.push({
            pathname: "command-game-video",
            query: {
                gameNumber,
                gameChoosen
            }
        })
        gameChoosen === "Blurry Game" && router.push({
            pathname: "command-game-blurry-game",
            query: {
                gameNumber,
                gameChoosen
            }
        })
        gameChoosen === "JDM" && router.push({
            pathname: "command-game-jdm",
            query: {
                gameNumber,
                gameChoosen
            }
        })
        gameChoosen === "QCM" && router.push({
            pathname: "command-game-qcm",
            query: {
                gameNumber,
                gameChoosen
            }
        })
        gameChoosen === "VOF" && router.push({
            pathname: "command-game-vof",
            query: {
                gameNumber,
                gameChoosen
            }
        })
        gameChoosen === "JDI" && router.push({
            pathname: "command-game-jdi",
            query: {
                gameNumber,
                gameChoosen
            }
        })
        gameChoosen === "Dezoom" && router.push({
            pathname: "command-game-dezoom",
            query: {
                gameNumber,
                gameChoosen
            }
        })
        gameChoosen === "Quizz" && router.push({
            pathname: "command-game-quizz",
            query: {
                gameNumber,
                gameChoosen
            }
        })
        gameChoosen === "Notanime" && router.push({
            pathname: "command-game-notanime",
            query: {
                gameNumber,
                gameChoosen
            }
        })
    }

    return (
        <div>
            <p>{messageError}</p>
            <h1>Liste des différents jeux</h1>
            {chooseGame && chooseGame.map((game: string) => <button className="button" onClick={() => getGameNumberList(game)} key={game}>{game}</button>)}
            <h2>Liste des jeux</h2>
            {chooseGameNumber &&
                chooseGameNumber.map((gameNumber: string) => {
                    const index = memory.findIndex(e => e.gameNumber === gameNumber)
                    if (index > -1) return <button className="button" key={gameNumber} onClick={() => gameListChoosen(gameNumber)}>{gameNumber} | {memory[index].date}</button>
                    return <button className="button" key={gameNumber} onClick={() => gameListChoosen(gameNumber)}>{gameNumber}</button>
                }
                )
            }
        </div>
    )
}

export async function getServerSideProps() {
    const REACT_APP_CLIENT_IP = process.env.REACT_APP_CLIENT_IP
    return {
        props: {
            REACT_APP_CLIENT_IP
        }
    }
}

export default Home
