import { NextPage } from "next";
import socketIOClient from "socket.io-client";
import { useRouter } from "next/router"
import { getGameJSONAPI } from "../../libs/API";
import { useEffect, useState } from "react";
import { convertQCMObject } from "../../libs/convert"
import ip from "ip";
let socket: any; // Déclaration globale

if (!socket) {
    socket = socketIOClient(`http://${process.env.NEXT_PUBLIC_REACT_APP_CLIENT_IP}:8888`);
    console.log("Socket.IO connecté :", socket.id);
}

{/* @ts-ignore */}
const CommandGameQCM: NextPage = ({ REACT_APP_CLIENT_IP }) => {

    const router = useRouter()
    const { gameNumber,gameChoosen } = router.query

    const [questions, setQuestions] = useState<[string] | []>([])
    const [launchGame, setLaunchGame] = useState<boolean>(false)
    const [numberMedia, setNumberMedia] = useState<number>(0)


    useEffect(() => {
        getGame()
    }, [])

    useEffect(() => {
        socket.emit("change-media", numberMedia,questions.length, gameNumber, gameChoosen)
    }, [numberMedia])

    async function getGame(): Promise<void> {
        const data = {
            //  @ts-ignore: Unreachable code error 
            jeu: await getGameJSONAPI("QCM", gameNumber, "localhost")
        }
        const realArray = convertQCMObject(data)
        console.log("🚀 ~ file: index.tsx ~ line 28 ~ getGame ~ realArray", realArray)
        {/* @ts-ignore: Unreachable code error */}
        setQuestions(realArray);
    }

    function launchGameFunc() {
        setLaunchGame(true)
        socket.emit("launchGame")
    }

    function menu() {
        socket.emit("return-menu")
        router.push({
            pathname: "/"
        })
    }

    function nextMedia() {
        setNumberMedia(numberMedia + 1)
    }

    function backMedia() {
        setNumberMedia(numberMedia - 1)
    }

    function showResponse() {
        socket.emit("show-response")
    }

    function showResponseFunc() {
        {/* @ts-ignore: Unreachable code error */}
        const res = questions[numberMedia].response.filter(propo => propo.ok === true)
        console.log(res)
        return res[0].proposal
    }

    return (<div>
        {!launchGame ?
            <><button className="button" onClick={() => launchGameFunc()}> Lancer le jeu </button> <button className="button" onClick={() => menu()}>retour au menu</button></>
            :
            <>
                {numberMedia < questions.length ? <p>Reponse : {showResponseFunc()} {numberMedia === questions.length - 1 && <>- c'est le dernier</>} </p> : <p>c'est fini</p>}
                <button className="button" onClick={() => nextMedia()}>Média suivant</button>
                {numberMedia != 0 && <button className="button" onClick={() => backMedia()}>Média précédent</button>}
                <button className="button" onClick={() => showResponse()}>Réponse</button>
                <button className="button" onClick={() => menu()}>retour au menu</button>
            </>}
    </div>)
}
export async function getServerSideProps() {
    const REACT_APP_CLIENT_IP = process.env.REACT_APP_CLIENT_IP
    return {
        props: {
            REACT_APP_CLIENT_IP
        }
    }
}
export default CommandGameQCM