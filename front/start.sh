#!/bin/bash

# Exécuter npm run build
echo "Building the project..."
npm run build

# Vérifier si le build a réussi
if [ $? -eq 0 ]; then
  echo "Build succeeded. Starting the project..."
  npm run start
else
  echo "Build failed. Exiting."
  exit 1
fi
