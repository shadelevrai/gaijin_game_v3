async function getGameMediaAPI(gameName: string, gameNumber: string, REACT_APP_CLIENT_IP: string) {
    const res = await fetch(`http://${REACT_APP_CLIENT_IP}:8888/game/${gameName}/${gameNumber}`);
    const data = await res.json();
    return data
}

async function getGameMediaAPIBlindTestChoice(REACT_APP_CLIENT_IP: string) {
    const res = await fetch(`http://${REACT_APP_CLIENT_IP}:8888/blindtestchoice`);
    const data = await res.json();
    return data
}

async function getGameJSONAPI(gameName: string, gameNumber: string, REACT_APP_CLIENT_IP: string) {
    const res = await fetch(`http://${REACT_APP_CLIENT_IP}:8888/game/json/${gameName}/${gameNumber}`);
    const data = await res.json()
    return data
}

async function getGameJSONAPIV2(gameName: string, gameNumber: string, REACT_APP_CLIENT_IP: string) {
    const res = await fetch(`http://${REACT_APP_CLIENT_IP}:8888/game/json/${gameName}/${gameNumber}`);
    const data = await res.json()
    console.log("🚀 ~ file: API.ts:16 ~ getGameJSONAPIV2 ~ data:", data)
    return data
}

async function getRandomMediaBlindTest( REACT_APP_CLIENT_IP: string) {
    const res = await fetch(`http://${REACT_APP_CLIENT_IP}:8888/random-media-blindtest`)
    const data = await res.json()
    return data
}

export { getGameMediaAPI, getGameJSONAPI, getRandomMediaBlindTest, getGameJSONAPIV2, getGameMediaAPIBlindTestChoice }