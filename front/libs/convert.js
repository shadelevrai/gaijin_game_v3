function convertQCMObject(data) {
    const realArray = [
        {
            question: null,
            response: [
                { proposal: null, ok: null },
                { proposal: null, ok: null },
                { proposal: null, ok: null },
                { proposal: null, ok: null },
            ],
        },
        {
            question: null,
            response: [
                { proposal: null, ok: null },
                { proposal: null, ok: null },
                { proposal: null, ok: null },
                { proposal: null, ok: null },
            ],
        },
        {
            question: null,
            response: [
                { proposal: null, ok: null },
                { proposal: null, ok: null },
                { proposal: null, ok: null },
                { proposal: null, ok: null },
            ],
        },
        {
            question: null,
            response: [
                { proposal: null, ok: null },
                { proposal: null, ok: null },
                { proposal: null, ok: null },
                { proposal: null, ok: null },
            ],
        },
        {
            question: null,
            response: [
                { proposal: null, ok: null },
                { proposal: null, ok: null },
                { proposal: null, ok: null },
                { proposal: null, ok: null },
            ],
        },
        {
            question: null,
            response: [
                { proposal: null, ok: null },
                { proposal: null, ok: null },
                { proposal: null, ok: null },
                { proposal: null, ok: null },
            ],
        },
        {
            question: null,
            response: [
                { proposal: null, ok: null },
                { proposal: null, ok: null },
                { proposal: null, ok: null },
                { proposal: null, ok: null },
            ],
        },
        {
            question: null,
            response: [
                { proposal: null, ok: null },
                { proposal: null, ok: null },
                { proposal: null, ok: null },
                { proposal: null, ok: null },
            ],
        },
        {
            question: null,
            response: [
                { proposal: null, ok: null },
                { proposal: null, ok: null },
                { proposal: null, ok: null },
                { proposal: null, ok: null },
            ],
        },
        {
            question: null,
            response: [
                { proposal: null, ok: null },
                { proposal: null, ok: null },
                { proposal: null, ok: null },
                { proposal: null, ok: null },
            ],
        },
        {
            question: null,
            response: [
                { proposal: null, ok: null },
                { proposal: null, ok: null },
                { proposal: null, ok: null },
                { proposal: null, ok: null },
            ],
        },
        {
            question: null,
            response: [
                { proposal: null, ok: null },
                { proposal: null, ok: null },
                { proposal: null, ok: null },
                { proposal: null, ok: null },
            ],
        },
        {
            question: null,
            response: [
                { proposal: null, ok: null },
                { proposal: null, ok: null },
                { proposal: null, ok: null },
                { proposal: null, ok: null },
            ],
        },
        {
            question: null,
            response: [
                { proposal: null, ok: null },
                { proposal: null, ok: null },
                { proposal: null, ok: null },
                { proposal: null, ok: null },
            ],
        },
        {
            question: null,
            response: [
                { proposal: null, ok: null },
                { proposal: null, ok: null },
                { proposal: null, ok: null },
                { proposal: null, ok: null },
            ],
        },
        {
            question: null,
            response: [
                { proposal: null, ok: null },
                { proposal: null, ok: null },
                { proposal: null, ok: null },
                { proposal: null, ok: null },
            ],
        },
        {
            question: null,
            response: [
                { proposal: null, ok: null },
                { proposal: null, ok: null },
                { proposal: null, ok: null },
                { proposal: null, ok: null },
            ],
        },
        {
            question: null,
            response: [
                { proposal: null, ok: null },
                { proposal: null, ok: null },
                { proposal: null, ok: null },
                { proposal: null, ok: null },
            ],
        },
        {
            question: null,
            response: [
                { proposal: null, ok: null },
                { proposal: null, ok: null },
                { proposal: null, ok: null },
                { proposal: null, ok: null },
            ],
        },
        {
            question: null,
            response: [
                { proposal: null, ok: null },
                { proposal: null, ok: null },
                { proposal: null, ok: null },
                { proposal: null, ok: null },
            ],
        },
    ];

    for (const [key, value] of Object.entries(data.jeu)) {
        //   console.log(`${key}: ${value}`);
        if (key === `question1`) {
            realArray[0].question = value;
        }
        if (key === `reponse1`) {
            for (const [key2, value2] of Object.entries(data.jeu.reponse1)) {
                if (key2 === "reponse01") {
                    realArray[0].response[0].proposal = value2.proposition;
                    realArray[0].response[0].ok = value2.reponse;
                }
                if (key2 === "reponse02") {
                    realArray[0].response[1].proposal = value2.proposition;
                    realArray[0].response[1].ok = value2.reponse;
                }
                if (key2 === "reponse03") {
                    realArray[0].response[2].proposal = value2.proposition;
                    realArray[0].response[2].ok = value2.reponse;
                }
                if (key2 === "reponse04") {
                    realArray[0].response[3].proposal = value2.proposition;
                    realArray[0].response[3].ok = value2.reponse;
                }
            }
        }
        if (key === `question2`) {
            realArray[1].question = value;
        }
        if (key === `reponse2`) {
            for (const [key2, value2] of Object.entries(data.jeu.reponse2)) {
                if (key2 === "reponse01") {
                    realArray[1].response[0].proposal = value2.proposition;
                    realArray[1].response[0].ok = value2.reponse;
                }
                if (key2 === "reponse02") {
                    realArray[1].response[1].proposal = value2.proposition;
                    realArray[1].response[1].ok = value2.reponse;
                }
                if (key2 === "reponse03") {
                    realArray[1].response[2].proposal = value2.proposition;
                    realArray[1].response[2].ok = value2.reponse;
                }
                if (key2 === "reponse04") {
                    realArray[1].response[3].proposal = value2.proposition;
                    realArray[1].response[3].ok = value2.reponse;
                }
            }
        }
        if (key === `question3`) {
            realArray[2].question = value;
        }
        if (key === `reponse3`) {
            for (const [key2, value2] of Object.entries(data.jeu.reponse3)) {
                if (key2 === "reponse01") {
                    realArray[2].response[0].proposal = value2.proposition;
                    realArray[2].response[0].ok = value2.reponse;
                }
                if (key2 === "reponse02") {
                    realArray[2].response[1].proposal = value2.proposition;
                    realArray[2].response[1].ok = value2.reponse;
                }
                if (key2 === "reponse03") {
                    realArray[2].response[2].proposal = value2.proposition;
                    realArray[2].response[2].ok = value2.reponse;
                }
                if (key2 === "reponse04") {
                    realArray[2].response[3].proposal = value2.proposition;
                    realArray[2].response[3].ok = value2.reponse;
                }
            }
        }
        if (key === `question4`) {
            realArray[3].question = value;
        }
        if (key === `reponse4`) {
            for (const [key2, value2] of Object.entries(data.jeu.reponse4)) {
                if (key2 === "reponse01") {
                    realArray[3].response[0].proposal = value2.proposition;
                    realArray[3].response[0].ok = value2.reponse;
                }
                if (key2 === "reponse02") {
                    realArray[3].response[1].proposal = value2.proposition;
                    realArray[3].response[1].ok = value2.reponse;
                }
                if (key2 === "reponse03") {
                    realArray[3].response[2].proposal = value2.proposition;
                    realArray[3].response[2].ok = value2.reponse;
                }
                if (key2 === "reponse04") {
                    realArray[3].response[3].proposal = value2.proposition;
                    realArray[3].response[3].ok = value2.reponse;
                }
            }
        }
        if (key === `question5`) {
            realArray[4].question = value;
        }
        if (key === `reponse5`) {
            for (const [key2, value2] of Object.entries(data.jeu.reponse5)) {
                if (key2 === "reponse01") {
                    realArray[4].response[0].proposal = value2.proposition;
                    realArray[4].response[0].ok = value2.reponse;
                }
                if (key2 === "reponse02") {
                    realArray[4].response[1].proposal = value2.proposition;
                    realArray[4].response[1].ok = value2.reponse;
                }
                if (key2 === "reponse03") {
                    realArray[4].response[2].proposal = value2.proposition;
                    realArray[4].response[2].ok = value2.reponse;
                }
                if (key2 === "reponse04") {
                    realArray[4].response[3].proposal = value2.proposition;
                    realArray[4].response[3].ok = value2.reponse;
                }
            }
        }
        if (key === `question6`) {
            realArray[5].question = value;
        }
        if (key === `reponse6`) {
            for (const [key2, value2] of Object.entries(data.jeu.reponse6)) {
                if (key2 === "reponse01") {
                    realArray[5].response[0].proposal = value2.proposition;
                    realArray[5].response[0].ok = value2.reponse;
                }
                if (key2 === "reponse02") {
                    realArray[5].response[1].proposal = value2.proposition;
                    realArray[5].response[1].ok = value2.reponse;
                }
                if (key2 === "reponse03") {
                    realArray[5].response[2].proposal = value2.proposition;
                    realArray[5].response[2].ok = value2.reponse;
                }
                if (key2 === "reponse04") {
                    realArray[5].response[3].proposal = value2.proposition;
                    realArray[5].response[3].ok = value2.reponse;
                }
            }
        }
        if (key === `question7`) {
            realArray[6].question = value;
        }
        if (key === `reponse7`) {
            for (const [key2, value2] of Object.entries(data.jeu.reponse7)) {
                if (key2 === "reponse01") {
                    realArray[6].response[0].proposal = value2.proposition;
                    realArray[6].response[0].ok = value2.reponse;
                }
                if (key2 === "reponse02") {
                    realArray[6].response[1].proposal = value2.proposition;
                    realArray[6].response[1].ok = value2.reponse;
                }
                if (key2 === "reponse03") {
                    realArray[6].response[2].proposal = value2.proposition;
                    realArray[6].response[2].ok = value2.reponse;
                }
                if (key2 === "reponse04") {
                    realArray[6].response[3].proposal = value2.proposition;
                    realArray[6].response[3].ok = value2.reponse;
                }
            }
        }
        if (key === `question8`) {
            realArray[7].question = value;
        }
        if (key === `reponse8`) {
            for (const [key2, value2] of Object.entries(data.jeu.reponse8)) {
                if (key2 === "reponse01") {
                    realArray[7].response[0].proposal = value2.proposition;
                    realArray[7].response[0].ok = value2.reponse;
                }
                if (key2 === "reponse02") {
                    realArray[7].response[1].proposal = value2.proposition;
                    realArray[7].response[1].ok = value2.reponse;
                }
                if (key2 === "reponse03") {
                    realArray[7].response[2].proposal = value2.proposition;
                    realArray[7].response[2].ok = value2.reponse;
                }
                if (key2 === "reponse04") {
                    realArray[7].response[3].proposal = value2.proposition;
                    realArray[7].response[3].ok = value2.reponse;
                }
            }
        }
        if (key === `question9`) {
            realArray[8].question = value;
        }
        if (key === `reponse9`) {
            for (const [key2, value2] of Object.entries(data.jeu.reponse9)) {
                if (key2 === "reponse01") {
                    realArray[8].response[0].proposal = value2.proposition;
                    realArray[8].response[0].ok = value2.reponse;
                }
                if (key2 === "reponse02") {
                    realArray[8].response[1].proposal = value2.proposition;
                    realArray[8].response[1].ok = value2.reponse;
                }
                if (key2 === "reponse03") {
                    realArray[8].response[2].proposal = value2.proposition;
                    realArray[8].response[2].ok = value2.reponse;
                }
                if (key2 === "reponse04") {
                    realArray[8].response[3].proposal = value2.proposition;
                    realArray[8].response[3].ok = value2.reponse;
                }
            }
        }
        if (key === `question10`) {
            realArray[9].question = value;
        }
        if (key === `reponse10`) {
            for (const [key2, value2] of Object.entries(data.jeu.reponse10)) {
                if (key2 === "reponse01") {
                    realArray[9].response[0].proposal = value2.proposition;
                    realArray[9].response[0].ok = value2.reponse;
                }
                if (key2 === "reponse02") {
                    realArray[9].response[1].proposal = value2.proposition;
                    realArray[9].response[1].ok = value2.reponse;
                }
                if (key2 === "reponse03") {
                    realArray[9].response[2].proposal = value2.proposition;
                    realArray[9].response[2].ok = value2.reponse;
                }
                if (key2 === "reponse04") {
                    realArray[9].response[3].proposal = value2.proposition;
                    realArray[9].response[3].ok = value2.reponse;
                }
            }
        }
        if (key === `question11`) {
            realArray[10].question = value;
        }
        if (key === `reponse11`) {
            for (const [key2, value2] of Object.entries(data.jeu.reponse11)) {
                if (key2 === "reponse01") {
                    realArray[10].response[0].proposal = value2.proposition;
                    realArray[10].response[0].ok = value2.reponse;
                }
                if (key2 === "reponse02") {
                    realArray[10].response[1].proposal = value2.proposition;
                    realArray[10].response[1].ok = value2.reponse;
                }
                if (key2 === "reponse03") {
                    realArray[10].response[2].proposal = value2.proposition;
                    realArray[10].response[2].ok = value2.reponse;
                }
                if (key2 === "reponse04") {
                    realArray[10].response[3].proposal = value2.proposition;
                    realArray[10].response[3].ok = value2.reponse;
                }
            }
        }
        if (key === `question12`) {
            realArray[11].question = value;
        }
        if (key === `reponse12`) {
            for (const [key2, value2] of Object.entries(data.jeu.reponse12)) {
                if (key2 === "reponse01") {
                    realArray[11].response[0].proposal = value2.proposition;
                    realArray[11].response[0].ok = value2.reponse;
                }
                if (key2 === "reponse02") {
                    realArray[11].response[1].proposal = value2.proposition;
                    realArray[11].response[1].ok = value2.reponse;
                }
                if (key2 === "reponse03") {
                    realArray[11].response[2].proposal = value2.proposition;
                    realArray[11].response[2].ok = value2.reponse;
                }
                if (key2 === "reponse04") {
                    realArray[11].response[3].proposal = value2.proposition;
                    realArray[11].response[3].ok = value2.reponse;
                }
            }
        }
        if (key === `question13`) {
            realArray[12].question = value;
        }
        if (key === `reponse13`) {
            for (const [key2, value2] of Object.entries(data.jeu.reponse13)) {
                if (key2 === "reponse01") {
                    realArray[12].response[0].proposal = value2.proposition;
                    realArray[12].response[0].ok = value2.reponse;
                }
                if (key2 === "reponse02") {
                    realArray[12].response[1].proposal = value2.proposition;
                    realArray[12].response[1].ok = value2.reponse;
                }
                if (key2 === "reponse03") {
                    realArray[12].response[2].proposal = value2.proposition;
                    realArray[12].response[2].ok = value2.reponse;
                }
                if (key2 === "reponse04") {
                    realArray[12].response[3].proposal = value2.proposition;
                    realArray[12].response[3].ok = value2.reponse;
                }
            }
        }
        if (key === `question14`) {
            realArray[13].question = value;
        }
        if (key === `reponse14`) {
            for (const [key2, value2] of Object.entries(data.jeu.reponse14)) {
                if (key2 === "reponse01") {
                    realArray[13].response[0].proposal = value2.proposition;
                    realArray[13].response[0].ok = value2.reponse;
                }
                if (key2 === "reponse02") {
                    realArray[13].response[1].proposal = value2.proposition;
                    realArray[13].response[1].ok = value2.reponse;
                }
                if (key2 === "reponse03") {
                    realArray[13].response[2].proposal = value2.proposition;
                    realArray[13].response[2].ok = value2.reponse;
                }
                if (key2 === "reponse04") {
                    realArray[13].response[3].proposal = value2.proposition;
                    realArray[13].response[3].ok = value2.reponse;
                }
            }
        }
        if (key === `question15`) {
            realArray[14].question = value;
        }
        if (key === `reponse15`) {
            for (const [key2, value2] of Object.entries(data.jeu.reponse15)) {
                if (key2 === "reponse01") {
                    realArray[14].response[0].proposal = value2.proposition;
                    realArray[14].response[0].ok = value2.reponse;
                }
                if (key2 === "reponse02") {
                    realArray[14].response[1].proposal = value2.proposition;
                    realArray[14].response[1].ok = value2.reponse;
                }
                if (key2 === "reponse03") {
                    realArray[14].response[2].proposal = value2.proposition;
                    realArray[14].response[2].ok = value2.reponse;
                }
                if (key2 === "reponse04") {
                    realArray[14].response[3].proposal = value2.proposition;
                    realArray[14].response[3].ok = value2.reponse;
                }
            }
        }
        if (key === `question16`) {
            realArray[15].question = value;
        }
        if (key === `reponse16`) {
            for (const [key2, value2] of Object.entries(data.jeu.reponse16)) {
                if (key2 === "reponse01") {
                    realArray[15].response[0].proposal = value2.proposition;
                    realArray[15].response[0].ok = value2.reponse;
                }
                if (key2 === "reponse02") {
                    realArray[15].response[1].proposal = value2.proposition;
                    realArray[15].response[1].ok = value2.reponse;
                }
                if (key2 === "reponse03") {
                    realArray[15].response[2].proposal = value2.proposition;
                    realArray[15].response[2].ok = value2.reponse;
                }
                if (key2 === "reponse04") {
                    realArray[15].response[3].proposal = value2.proposition;
                    realArray[15].response[3].ok = value2.reponse;
                }
            }
        }
        if (key === `question17`) {
            realArray[16].question = value;
        }
        if (key === `reponse17`) {
            for (const [key2, value2] of Object.entries(data.jeu.reponse17)) {
                if (key2 === "reponse01") {
                    realArray[16].response[0].proposal = value2.proposition;
                    realArray[16].response[0].ok = value2.reponse;
                }
                if (key2 === "reponse02") {
                    realArray[16].response[1].proposal = value2.proposition;
                    realArray[16].response[1].ok = value2.reponse;
                }
                if (key2 === "reponse03") {
                    realArray[16].response[2].proposal = value2.proposition;
                    realArray[16].response[2].ok = value2.reponse;
                }
                if (key2 === "reponse04") {
                    realArray[16].response[3].proposal = value2.proposition;
                    realArray[16].response[3].ok = value2.reponse;
                }
            }
        }
        if (key === `question18`) {
            realArray[17].question = value;
        }
        if (key === `reponse18`) {
            for (const [key2, value2] of Object.entries(data.jeu.reponse18)) {
                if (key2 === "reponse01") {
                    realArray[17].response[0].proposal = value2.proposition;
                    realArray[17].response[0].ok = value2.reponse;
                }
                if (key2 === "reponse02") {
                    realArray[17].response[1].proposal = value2.proposition;
                    realArray[17].response[1].ok = value2.reponse;
                }
                if (key2 === "reponse03") {
                    realArray[17].response[2].proposal = value2.proposition;
                    realArray[17].response[2].ok = value2.reponse;
                }
                if (key2 === "reponse04") {
                    realArray[17].response[3].proposal = value2.proposition;
                    realArray[17].response[3].ok = value2.reponse;
                }
            }
        }
        if (key === `question19`) {
            realArray[18].question = value;
        }
        if (key === `reponse19`) {
            for (const [key2, value2] of Object.entries(data.jeu.reponse19)) {
                if (key2 === "reponse01") {
                    realArray[18].response[0].proposal = value2.proposition;
                    realArray[18].response[0].ok = value2.reponse;
                }
                if (key2 === "reponse02") {
                    realArray[18].response[1].proposal = value2.proposition;
                    realArray[18].response[1].ok = value2.reponse;
                }
                if (key2 === "reponse03") {
                    realArray[18].response[2].proposal = value2.proposition;
                    realArray[18].response[2].ok = value2.reponse;
                }
                if (key2 === "reponse04") {
                    realArray[18].response[3].proposal = value2.proposition;
                    realArray[18].response[3].ok = value2.reponse;
                }
            }
        }
        if (key === `question20`) {
            realArray[19].question = value;
        }
        if (key === `reponse20`) {
            for (const [key2, value2] of Object.entries(data.jeu.reponse20)) {
                if (key2 === "reponse01") {
                    realArray[19].response[0].proposal = value2.proposition;
                    realArray[19].response[0].ok = value2.reponse;
                }
                if (key2 === "reponse02") {
                    realArray[19].response[1].proposal = value2.proposition;
                    realArray[19].response[1].ok = value2.reponse;
                }
                if (key2 === "reponse03") {
                    realArray[19].response[2].proposal = value2.proposition;
                    realArray[19].response[2].ok = value2.reponse;
                }
                if (key2 === "reponse04") {
                    realArray[19].response[3].proposal = value2.proposition;
                    realArray[19].response[3].ok = value2.reponse;
                }
            }
        }
    }
    return realArray
}

export {convertQCMObject}