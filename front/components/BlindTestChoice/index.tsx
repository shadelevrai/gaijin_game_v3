import socketIOClient from "socket.io-client";
import { useEffect, useState } from "react";
import ReactPlayer from "react-player/lazy";
import { useRouter } from "next/router"
import ReseauxSociaux from "../ReseauxSociaux";
import StateNumber from "../StateNumber";
import ReseauxSociauxCadre from "../ReseauxSociauxCadre";

const socket = socketIOClient("http://localhost:8888");

const BlindTestChoice = () => {

    const router = useRouter()

    const [mediaChoice, setMediaChoice] = useState<string>("")
    const [play, setPlay] = useState<boolean>(true)
    const [volume, setVolume] = useState<number>(0.5);
    const [showReponse, setShowResponse] = useState<boolean>(false)
    const [numberMedia, setNumberMedia] = useState<number>(0);
    const [isOver, setIsOver] = useState<boolean>(false);

    useEffect(() => {
        socket.on("next-media-blind-test-choice", mediaChoice => {
            setMediaChoice(mediaChoice)
            setVolume(0.5);
            setShowResponse(false);
        })
        socket.on("pause", () => {
            setPlay(false)
        })
        socket.on("play", () => {
            setPlay(true)
        })
        socket.on("return-menu", () => {
            router.reload()
        })
        socket.on("show-response", () => {
            setShowResponse(true)
        })
        socket.on("blind-test-choice-over", () => {
            setIsOver(true)
        })
    }, [])

    useEffect(() => {
        socket.on("volume-up", () => {
            volume < 0.9 && setVolume(volume + 0.1)
        })
        socket.on("volume-down", () => {
            volume > 0.1 && setVolume(volume - 0.1)
        })
    }, [volume])

    useEffect(() => {
        mediaChoice && setNumberMedia(numberMedia => numberMedia + 1);
    }, [mediaChoice])

    function formatResponse() {
        const num = mediaChoice.indexOf("/");
        return mediaChoice.substring(num + 4, mediaChoice.length - 4);
    }

    return (<div>
        {!mediaChoice && !isOver && <>
            <p className="titleBlindTest">Blind Test</p>
            <hr className="lineundertitleblindtest" />
            <img src="../img/logo.png" style={{ position: "absolute", width: "20vw", marginLeft: "40vw", marginTop: "60vh" }} />
            <div style={{ "position": "absolute", "marginLeft": "75vw", "marginTop": "80vh" }}>
                <ReseauxSociaux />
            </div>
        </>}
        {mediaChoice && !isOver && <>
            <div className="backgroundBlindTest">
                <div className="lecteurBlindtest">
                    <ReactPlayer
                        url={`media/Blind Test/${mediaChoice}`}
                        playing={play}
                        volume={volume}
                        width="70vw"
                        height="70vh"
                    />
                </div>
                {!showReponse && <img src="../img/giphy.gif" alt="img" className="gifBlindtest" />}
                {showReponse && <>
                    <div style={{ position: "absolute", marginTop: "80vh", width: "100%" }}>
                        <p className="has-text-centered responseBlindtest"> {formatResponse()} </p>
                    </div>
                </>}
                <StateNumber numberMedia={numberMedia} />
            </div>
        </>}
        {isOver && <>
            <figure className="image is-fullwidth">
                    <img src="../img/over.png" alt="img" />
                </figure>
                <div className="columns mt-5">
                    <div className="column is-6 is-offset-3 has-text-centered">
                        {/* @ts-ignore: Unreachable code error */}
                        <ReseauxSociauxCadre />
                    </div>
                </div>
        </>}


    </div>)
}

export default BlindTestChoice