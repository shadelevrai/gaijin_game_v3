import { FunctionComponent } from "react";

interface Props {
    cadre:boolean
}

const ReseauxSociauxCadre: FunctionComponent<Props> = ({ cadre = true }) => {
    return (
        // @ts-ignore: Unreachable code error
        <div className="" style={{ "width": "50vw", "height": "30vh", "border": cadre ? "2px solid rgb(7,125,235)" : null }} >
            <div className="columns">
                <div className="column is-3 has-text-centered">
                    <img src="../img/insta.png" className="logoInstaLarge ml-4" style={{ "marginTop": "6vh"/*, "marginLeft": "5vw" */ }} />
                </div>
                <div className="column is-8" style={{marginTop:"5vh"}}>
                    <p className="has-text-weight-semibold join" style={{fontSize:"1.9vw"}}>REJOIGNEZ-NOUS SUR INSTAGRAM</p>
                    <p className="has-text-weight-bold" style={{fontSize:"4vw"}}>@gaijin_team</p>
                </div>

            </div>
        </div>
    )
}

export default ReseauxSociauxCadre