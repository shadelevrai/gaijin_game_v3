import { FunctionComponent, useEffect, useState } from "react";
import socketIOClient from "socket.io-client";
import { useRouter } from "next/router"
import queryString from "query-string";
import { getGameJSONAPIV2 } from "../../libs/API";
import ReseauxSociaux from "../ReseauxSociaux";

const socket = socketIOClient("http://localhost:8888");

interface Props {
    gameNumber: string
}


const Quizz: FunctionComponent<Props> = ({ gameNumber }) => {

    const [launchGame, setLaunchGame] = useState<boolean>(false)
    const [numberMedia, setNumberMedia] = useState<number>(0)
    const [questions, setQuestions] = useState<[]>([]);
    const [showResponse, setShowResponse] = useState<boolean>(false);

    const router = useRouter()
    const { game } = queryString.parse(location.search);

    useEffect(() => {
        getGame()
        socket.on("change-media", numberMedia => {
            setNumberMedia(numberMedia)
            setShowResponse(false)
        })
        socket.on("show-response", () => {
            setShowResponse(true)
        })
        socket.on("launchGame", () => {
            setLaunchGame(true)
        })
        socket.on("return-menu", () => {
            router.reload()
        })
    }, [])

    async function getGame() {
        const data = {
            jeu: await getGameJSONAPIV2("Quizz", gameNumber, "localhost")
        }
      
        {/* @ts-ignore */}
        setQuestions(data.jeu);

    }

    useEffect(()=>{
        console.log("🚀 ~ file: index.tsx:55 ~ questions:", questions)
    },[questions])

    return (
        <div>
            {!launchGame && <>
            <img src="../img/backgroundQuizz.png" className="backgroundvof" />
                <div>
                    {/* <p className="titlevof">Vrai ou faux</p> */}
                    {/* <img className="logovof" src="../img/logo.png" alt="img" /> */}
                </div>

            </>}
            {launchGame && questions.length !== numberMedia && <>
                <img src="../img/backgroundQuizzGoGame.png" className="backgroundvof" />
                <p className="numberquizz">
                    {numberMedia + 1} / {questions.length}
                </p>
                <p className="titlevof2"></p>
                {/*  @ts-ignore */}
                <p className="questionquizz"> {questions[numberMedia].question} </p>
                {/* @ts-ignore */}
                {showResponse && <p className="responsequizz"> {questions[numberMedia].response} </p>}
                {/* @ts-ignore */}
                {/* {showResponse && <p className="responsevof2"> {questions[numberMedia].response.slice(5)} </p>} */}
                <img className="logovof2" src="../img/logo.png" alt="img" />
                <div style={{ "position": "absolute", "marginTop": "90vh", "marginLeft": "75vw" }}>
                    {/* <ReseauxSociaux /> */}
                </div>


            </>}
            {questions.length === numberMedia && <>
                <img src="../img/backgroundQuizz.png" className="backgroundvof" />
            </>}
        </div>
    )
}

export default Quizz