import { FunctionComponent, useEffect, useState } from "react";
import socketIOClient from "socket.io-client";
import { useRouter } from "next/router"
import { getGameMediaAPI } from "../../libs/API";
import { decode } from 'html-entities';

const socket = socketIOClient("http://localhost:8888");

interface Props {
    gameNumber: string
}

const JDI: FunctionComponent<Props> = ({ gameNumber }) => {

    const [mediaLinks, getMediaLinks] = useState<[]>([]);
    const [numberMedia, setNumberMedia] = useState<number>(0)
    const [launchGame, setLaunchGame] = useState<boolean>(false)
    const [showResponse, setShowResponse] = useState<boolean>(false);

    const router = useRouter()

    useEffect(() => {
        getGame()
        socket.on("return-menu", () => {
            router.reload()
        })
        socket.on("change-media", numberMedia => {
            setNumberMedia(numberMedia)
            setShowResponse(false)
        })
        socket.on("launchGame", () => {
            setLaunchGame(true)
        })
        socket.on("show-response", () => {
            setShowResponse(true)
        })
    }, [])

    useEffect(() => {
        console.log("🚀 ~ file: index.tsx:41 ~ mediaLinks:", mediaLinks)
    }, [mediaLinks])

    async function getGame() {
        const mediaLinks = await getGameMediaAPI("JDI", gameNumber, "localhost");
        mediaLinks[0].slice(3, 7) != "EYES" && mediaLinks.sort(() => Math.random() - 0.5);
        getMediaLinks(mediaLinks);
    }


    return (<div>
        <img className="backgroundIMGjdi" src="../img/backJDI22.png" alt="img" />
        {!launchGame && <>

            <img className="logojdi" src="../img/logo.png" alt="img" />
            <p className="tutojdi">Devinez d'où provient l'image</p>
            <h1 className="title titrejdi">Jeu des images</h1>

        </>}
        {launchGame && mediaLinks.length !== numberMedia && <>
            {/* @ts-ignore: Unreachable code error */}
            {mediaLinks[numberMedia].slice(3, 7) === "EYES" ? <>
                <img className="imagejdi" src={`../media/JDI/${gameNumber}/${decode(mediaLinks[numberMedia])}`} alt="img" />
                  
                <p className="numbervof">
                    {numberMedia + 1} / {mediaLinks.length}
                </p>
                {/* @ts-ignore */}
                {numberMedia %2 === 1 && mediaLinks[numberMedia].length < 44 && <p className="responsejdi"> {mediaLinks[numberMedia].slice(8, -4)} </p>}
                {/* @ts-ignore */}
                {numberMedia %2 === 1 && mediaLinks[numberMedia].length > 43 && <p className="responsejdi2"> {mediaLinks[numberMedia].slice(8, -4)} </p>}
            </>
                :
                <>
                    <img className="imagejdi" src={`../media/JDI/${gameNumber}/${mediaLinks[numberMedia]}`} alt="img" />
                    <p className="numbervof">
                        {numberMedia + 1} / {mediaLinks.length}
                    </p>
                    {/* @ts-ignore */}
                    {showResponse && mediaLinks[numberMedia].length < 44 && <p className="responsejdi"> {mediaLinks[numberMedia].slice(0, -4)} </p>}
                    {/* @ts-ignore */}
                    {showResponse && mediaLinks[numberMedia].length > 43 && <p className="responsejdi2"> {mediaLinks[numberMedia].slice(0, -4)} </p>}</>}

        </>}
        {mediaLinks.length === numberMedia && <>
            <p className="thank" style={{ "color": "black" }}>Merci d'avoir joué</p>
        </>}
    </div>)
}

export default JDI