import { FunctionComponent } from "react"

interface Props {
    numberMedia: number
}

const StateNumber: FunctionComponent<Props> = ({numberMedia}) => {
    return (
        <div className="stateNumberBlindtest">{numberMedia < 10 ? <p>0{numberMedia}</p> : <p> {numberMedia} </p>}</div>
    )
}

export default StateNumber