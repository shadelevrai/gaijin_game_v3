import { FunctionComponent } from "react";

interface Props {
    response: string
}

const ShowResponse: FunctionComponent<Props> = ({ response }) => {
    return (
        <p className="responseBlindtest">{response.slice(3, -4)}</p>
    )
}

export default ShowResponse