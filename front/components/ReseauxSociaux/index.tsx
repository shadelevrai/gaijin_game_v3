const ReseauxSociaux = () => {
   return (
      <>
         <div className="columns">
            <img src="../img/insta_red.png" className="logoInsta" style={{ width: "3vw" }} />
            <p className="has-text-weight-bold" style={{ marginLeft: "0.5vw", fontSize: "2vw", color: "red" }}>Gaijin_team</p>
         </div>
      </>
   )
}

export default ReseauxSociaux