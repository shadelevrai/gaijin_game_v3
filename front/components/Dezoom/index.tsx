import { FunctionComponent, useState, useEffect } from "react";
import socketIOClient from "socket.io-client";
import { getGameMediaAPI } from "../../libs/API";
import { useRouter } from "next/router"
import ReseauxSociauxCadre from "../ReseauxSociauxCadre";
const socket = socketIOClient("http://localhost:8888");

interface Props {
    gameNumber: string
}

const Dezoom: FunctionComponent<Props> = ({ gameNumber }) => {

    const router = useRouter()

    const [mediaLinks, getMediaLinks] = useState<[]>([])
    const [numberMedia, setNumberMedia] = useState<number>(0)
    const [launchGame, setLaunchGame] = useState<boolean>(false)
    const [counter, setCounter] = useState<number>(50)
    const [showReponse, setShowResponse] = useState<boolean>(false)

    useEffect(() => {
        getGame()
        socket.on("change-media", numberMedia => {
            setNumberMedia(numberMedia)
            setShowResponse(false)
            setCounter(50)
        })
        socket.on("return-menu", () => router.reload())
        socket.on("launchGame", () => setLaunchGame(true))
        socket.on("show-response", () => {
            setShowResponse(true)
            setCounter(1)
        })
    }, [])

    useEffect(() => {
        counter > 0 && socket.on("less-Blur-2", () => setCounter(counter - 2)), socket.on("less-Blur-1", () => setCounter(counter - 1))
    }, [counter])


    async function getGame() {
        const mediaLinks = await getGameMediaAPI("Dezoom", gameNumber, "localhost");
        getMediaLinks(mediaLinks);
    }

    return (
        <div>
            {!launchGame && <>
                <img className="backgroundIMGBlurryGame" src="../img/back.jpg" alt="img" />
                <p className="titleBlurryGame">Dezoom</p>
                <hr className="lineUnderTitleBlurryGame" />
                <p className="tutoBlurryGame">Une image trop zoomée et un compteur va apparaître. Quand vous reconnaissez le titre, levez la main. Si vous avez la bonne réponse, vous optenez le score du compteur.</p>
                <img src="../img/logo.png" style={{ position: "absolute", width: "20vw", marginLeft: "40vw", marginTop: "70vh" }} />
            </>}
            {launchGame && mediaLinks.length !== numberMedia && <>
                <img className="backgroundIMGBlurryGame" src="../img/back.jpg" alt="img" />
                <div className="wrap">
                    <img className="blurryImgBlurryGame2" src={`/media/Dezoom/${gameNumber}/${mediaLinks[numberMedia]}`} alt="img" style={{transform:`scale(${counter>1?counter:1})`}} />
                </div>
                <p className="counterBlurryGame"> {counter} </p>
                <div className="stateNumberBlurryGame">{numberMedia + 1 < 10 ? <p>0{numberMedia + 1}</p> : <p> {numberMedia + 1} </p>}</div>
                <p className="totalNumberBlurryGame">/{mediaLinks.length}</p>
                {showReponse ?
                    <div style={{ position: "absolute", marginTop: "80vh", width: "100%" }}>
                        {/* @ts-ignore: Unreachable code error */}
                        <p className="has-text-centered responseBlurryGame">{mediaLinks[numberMedia].slice(3, -4)}</p>
                    </div>
                    :
                    <img src="../img/logo.png" alt="img" className="logoBlurryGame" />}
            </>}
            {numberMedia + 1 > mediaLinks.length && <>
                <figure className="image is-fullwidth">
                    <img src="../img/over.png" alt="img" />
                </figure>
                <div className="columns mt-5">
                    <div className="column is-6 is-offset-3 has-text-centered">
                        {/* @ts-ignore: Unreachable code error */}
                        <ReseauxSociauxCadre />
                    </div>
                </div>
            </>}
        </div>
    )
}

export default Dezoom