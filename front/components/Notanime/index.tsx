import { useRouter } from "next/router"
import { FunctionComponent, useEffect, useState } from "react";
import { getGameJSONAPI } from "../../libs/API";
import socketIOClient from "socket.io-client";

const socket = socketIOClient("http://localhost:8888");

interface Props {
    gameNumber: string
}

const Notanime: FunctionComponent<Props> = ({ gameNumber }) => {

    const router = useRouter()

    const [questions, setQuestions] = useState()
    const [launchGame, setLaunchGame] = useState<boolean>(false)
    // const [numberMedia, setNumberMedia] = useState<number>(0)
    const [showReponse, setShowResponse] = useState<boolean>(false)
    const [numb,setNumb] = useState(0)

    useEffect(() => {
        getGame()
        socket.on("change-media", () => {
            setShowResponse(false)
            setNumb(numb=>numb+2)
        })
        socket.on("return-menu", () => {
            router.reload()
        })
        socket.on("launchGame", () => {
            setLaunchGame(true)
        })
        socket.on("show-response", () => {
            setShowResponse(true)
        })
    }, [])

    function shuffleArray(array) {
        return array.sort(() => Math.random() - 0.5);
    }

    async function getGame() {
        const data = {
            jeu: await getGameJSONAPI("Notanime", gameNumber, "localhost")
        }
        console.log("🚀 ~ getGame ~ data:", data)
        setQuestions(shuffleArray(data.jeu));
    }

    useEffect(() => {
        console.log(questions)
    }, [questions])

    return (
        <div>
            {!launchGame && <img src="../img/backneutral.png" alt="imagefondqcm" className="imgbackgroundqcm" />}

            {launchGame &&
                <div>
                    {!showReponse && <img src="../img/backneutral.png" alt="imagefondqcm" className="imgbackgroundqcm" />}
                    {showReponse && <>
                        {questions[numb].score > questions[numb+1].score ? <img src="../img/backredright.png" alt="imagefondqcm" className="imgbackgroundqcm" /> : <img src="../img/backredleft.png" alt="imagefondqcm" className="imgbackgroundqcm" />}
                    </>}
                    <div className="cadretitle1">
                        <p className={questions[numb].name.length < 30 ? "title1" : "titile1small"}>
                            {questions[numb].name}
                        </p>
                        <p className="descr1"> {questions[numb].information} </p>
                    </div>
                    <div className="cadretitle2">
                        <p className={questions[numb+1].name.length < 30 ? "title2" : "titile2small"}>
                            {questions[numb+1].name}
                        </p>
                        <p className="descr2"> {questions[numb+1].information} </p>

                    </div>
                    {showReponse && <>
                    
                        <div className="cadrereponse1">
                            <p>{questions[numb].score}</p>
                        </div>
                        <div className="cadrereponse2">
                            <p> {questions[numb+1].score}</p>
                        </div>
                    </>}
                </div>
            }
        </div>
    )
}

export default Notanime