import { FunctionComponent, useEffect, useState } from "react";
import socketIOClient from "socket.io-client";
import { useRouter } from "next/router"
import { getGameJSONAPI } from "../../libs/API";
import ReseauxSociaux from "../ReseauxSociaux";

const socket = socketIOClient("http://localhost:8888");

interface Props {
    gameNumber: string
}

const JDM: FunctionComponent<Props> = ({ gameNumber }) => {
    const router = useRouter()

    const [words, setWords] = useState<[string] | []>([])
    const [numberMedia, setNumberMedia] = useState<number>(0)
    const [launchGame, setLaunchGame] = useState<boolean>(false)
    const [showReponse, setShowResponse] = useState<boolean>(false)
    const [numberIndice, setNumberIndice] = useState<number>(1);

    useEffect(() => {
        getGame()
        socket.on("launchGame", () => {
            setLaunchGame(true)
        })
        socket.on("show-response", () => {
            setShowResponse(true)
        })
        socket.on("return-menu", () => {
            router.reload()
        })
        socket.on("change-media", numberMedia => {
            setNumberMedia(numberMedia)
            setShowResponse(false)
            setNumberIndice(1)
        })
    }, [])

    useEffect(() => {
        socket.on("next-indice", () => {
            numberIndice < 10 && setNumberIndice(numberIndice + 1)
        })
    }, [numberIndice])

    async function getGame(): Promise<void> {
        const mediaLinks = await getGameJSONAPI("JDM", gameNumber, "localhost");
        setWords(mediaLinks);
    }

    return (
        <>
            <img className="backgroundjdm" src="../img/backgroundjdm.jpg" alt="imagejdm" />
            {!launchGame && <>
                <h1 className="titlejdm">Jeux des mots</h1>
                {/* <img className="logojdm" src="../img/logo.png" alt="img" /> */}
                <div style={{ "position": "absolute", "marginLeft": "75vw", "marginTop": "80vh" }}>
                    <ReseauxSociaux />
                </div>
                <p className="tutojdm">Plus vous avez besoin d'indices pour trouver un titre, moins vous gagnerez de points</p>

            </>}
            {launchGame && numberMedia < words.length && <>
                <p className="statejdm">{numberMedia + 1}</p>
                 {/* @ts-ignore: Unreachable code error */}
                {showReponse ? <p className="responsejdm">{words[numberMedia].words[0]}</p> : <p className="noresponsejdm">???</p>}

                {numberIndice === 1 && <p className="pointjdm">9 Points</p>}
                {/* @ts-ignore: Unreachable code error */}
                <p className="word1">{words[numberMedia].words[1]} </p>
                {numberIndice === 2 && <p className="pointjdm">8 Points</p>}
                {/* @ts-ignore: Unreachable code error */}
                {numberIndice > 1 && <p className="word2"> {words[numberMedia].words[2]} </p>}
                {numberIndice === 3 && <p className="pointjdm">7 Points</p>}
                {/* @ts-ignore: Unreachable code error */}
                {numberIndice > 2 && <p className="word3"> {words[numberMedia].words[3]} </p>}
                {numberIndice === 4 && <p className="pointjdm">6 Points</p>}
                {/* @ts-ignore: Unreachable code error */}
                {numberIndice > 3 && <p className="word4"> {words[numberMedia].words[4]} </p>}
                {numberIndice === 5 && <p className="pointjdm">5 Points</p>}
                {/* @ts-ignore: Unreachable code error */}
                {numberIndice > 4 && <p className="word5"> {words[numberMedia].words[5]} </p>}
                {/* @ts-ignore: Unreachable code error */}
                {numberIndice === 6 && <p className="pointjdm">4 Points</p>}
                {/* @ts-ignore: Unreachable code error */}
                {numberIndice > 5 && <p className="word6"> {words[numberMedia].words[6]} </p>}
                {numberIndice === 7 && <p className="pointjdm">3 Points</p>}
                {/* @ts-ignore: Unreachable code error */}
                {numberIndice > 6 && <p className="word7"> {words[numberMedia].words[7]} </p>}
                {numberIndice === 8 && <p className="pointjdm">2 Points</p>}
                {/* @ts-ignore: Unreachable code error */}
                {numberIndice > 7 && <p className="word8"> {words[numberMedia].words[8]} </p>}
                {numberIndice === 9 && <p className="pointjdm">1 Points</p>}
                {/* @ts-ignore: Unreachable code error */}
                {numberIndice > 8 && <p className="word9"> {words[numberMedia].words[9]} </p>}
                {numberIndice === 10 && <p className="pointjdm">0 Points</p>}
                {/* @ts-ignore: Unreachable code error */}
                {numberIndice > 9 && <p className="word10"> {words[numberMedia].words[10]} </p>}
                <div style={{ "position": "absolute", "marginLeft": "75vw", "marginTop": "85vh" }}>
                    <ReseauxSociaux />
                </div>
            </>}
            {launchGame && numberMedia === words.length && <>
                <div>
                    <p className="thanksjdm">Merci d'avoir participé</p>
                    <img className="logojdmthanks" src="../img/logo.png" alt="img" />
                </div>

            </>}
        </>
    )
}

export default JDM