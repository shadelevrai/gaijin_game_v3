import { FunctionComponent, useEffect, useState } from "react";
import socketIOClient from "socket.io-client";
import { getGameMediaAPI } from "../../libs/API";
import ReactPlayer from "react-player/lazy";
import { useRouter } from "next/router"
import ReseauxSociaux from "../ReseauxSociaux";
import StateNumber from "../StateNumber";
import ReseauxSociauxCadre from "../ReseauxSociauxCadre";

const socket = socketIOClient("http://localhost:8888");

interface Props {
    gameNumber: string
}

const BlindTest: FunctionComponent<Props> = ({ gameNumber }) => {

    const regex = new RegExp("demi-mute");

    const router = useRouter()

    const [mediaLinks, getMediaLinks] = useState<[]>([])
    const [numberMedia, setNumberMedia] = useState<number>(0)
    const [showReponse, setShowResponse] = useState<boolean>(false)
    const [play, setPlay] = useState<boolean>(false)
    const [launchGame, setLaunchGame] = useState<boolean>(false)
    const [volume, setVolume] = useState<number>(0.5);
    const [demiMute, setDemiMute] = useState<boolean>(false)
    const [random, setRandom] = useState(false)

    useEffect(() => {
        getGame()
        socket.on("change-media", numberMedia => {
            setNumberMedia(numberMedia)
            setShowResponse(false)
            setVolume(0.5)
            console.log("Le bouton CHANGEMEDIA a été reçu par le serveur")
        })
        socket.on("show-response", () => {
            console.log("Le bouton SHOWREPONSE a été reçu par le serveur")
            setShowResponse(true)
        })
        socket.on("play", () => {
            console.log("Le bouton PLAY a été reçu par le serveur")
            setPlay(true)
        })
        socket.on("pause", () => {
            console.log("Le bouton PAUSE a été reçu par le serveur")
            setPlay(false)
        })
        socket.on("launchGame", () => {
            console.log("Le bouton LAUNCHGAME a été reçu par le serveur")
            setLaunchGame(true)
        })
        socket.on("return-menu", () => {
            console.log("Le bouton RETURNMENU a été reçu par le serveur")
            router.reload()
        })
    }, [])

    useEffect(() => {
        socket.on("volume-up", () => {
            volume < 0.9 && setVolume(volume + 0.1)
        })
        socket.on("volume-down", () => {
            volume > 0.1 && setVolume(volume - 0.1)
        })
    }, [volume])

    async function getGame() {
        if (gameNumber === "blind test 00 RANDOM") {
            setRandom(true)
            setTimeout(() => {
                getMediaRandom()
            }, 3000);
        } else {
            const mediaLinks = await getGameMediaAPI("Blind Test", gameNumber, "localhost");
            // if (new RegExp("demi-mute")) {
            //     setTimeout(() => {
            //         demiMute ? setDemiMute(false) : setDemiMute(true)
            //     }, 2000);
            // }
            getMediaLinks(mediaLinks);
        }
    }

    async function getMediaRandom() {
        const res = await fetch(`http://localhost:8888/test`)
        const resJson = await res.json()
        getMediaLinks(resJson)
    }

    setTimeout(() => {
        demiMute ? setDemiMute(false) : setDemiMute(true)
    }, 1500);

    function sliceRandomMedia() {
        // @ts-ignore: Unreachable code error 
        const texte = mediaLinks[numberMedia].link
        let position = texte.indexOf("/");
        let deuxLettres = texte.slice(position+3, -4);
        return deuxLettres
    }

    function slinceRandomMediaShow(){
        // @ts-ignore: Unreachable code error 
        const texte = mediaLinks[numberMedia].link
        let position = texte.indexOf("/");
        let deuxLettres = texte.slice(position+4,position+8);
        return deuxLettres
    }

    function sliceWithoutShow() {
         // @ts-ignore: Unreachable code error 
         const texte = mediaLinks[numberMedia].link
         let position = texte.indexOf("/");
         let deuxLettres = texte.slice(position+9,-4);
         return deuxLettres
    }

    return (
        <div>
            {!launchGame && <div>
                <p className="titleBlindTest">Blind Test</p>
                <hr className="lineundertitleblindtest" />
                <img src="../img/logo.png" style={{ position: "absolute", width: "20vw", marginLeft: "40vw", marginTop: "60vh" }} />
                <div style={{ "position": "absolute", "marginLeft": "75vw", "marginTop": "80vh" }}>
                    <ReseauxSociaux />
                </div>
            </div>}
            {launchGame && mediaLinks.length !== numberMedia && <>
                <img src="../img/large.png" alt="img" className="leftgirlBlindtest" />
                <img src="../img/neko.png" alt="img" className="rightgirlBlindtest" />
                <div style={{ "position": "absolute", "marginTop": "90vh", "marginLeft": "78vw" }}>
                    <ReseauxSociaux />
                </div>
                {mediaLinks.length > 0 && numberMedia + 1 <= mediaLinks.length &&
                    <div className="backgroundBlindTest">
                        <div className="lecteurBlindtest">
                            {/* {!showReponse && <ReactPlayer url={`media/Blind Test/${gameNumber}/${mediaLinks[numberMedia]}`} width="70vw" height="70vh" playing={play} onReady={() => setPlay(true)} onEnded={() => setPlay(false)} volume={volume} muted={regex.test(gameNumber) ? demiMute : false} />} */}
                            <ReactPlayer
                            // @ts-ignore: Unreachable code error
                                url={random ? `media/Blind Test/${mediaLinks[numberMedia].link}` : `media/Blind Test/${gameNumber}/${mediaLinks[numberMedia]}`}
                                // url={`media/Blind Test/${gameNumber}/${mediaLinks[numberMedia]}`} 
                                width="70vw"
                                height="70vh"
                                playing={play}
                                onReady={() => setPlay(true)} onEnded={() => setPlay(false)} volume={volume} />

                        </div>
                        {/* @ts-ignore: Unreachable code error */}
                        {random ?
                            // @ts-ignore: Unreachable code error
                            !showReponse && slinceRandomMediaShow() != "SHOW" && <img src="../img/giphy.gif" alt="img" className="gifBlindtest" />
                            :
                            // @ts-ignore: Unreachable code error
                            !showReponse && mediaLinks[numberMedia].slice(3, 7) != "SHOW" && <img src="../img/giphy.gif" alt="img" className="gifBlindtest" />
                        }


                        <StateNumber numberMedia={numberMedia + 1} />
                        <p className="totalNumberBlindtest">/{mediaLinks.length}</p>
                        {showReponse ?
                            <div style={{ position: "absolute", marginTop: "80vh", width: "100%" }}>
                                {/* <ShowResponse response={mediaLinks[numberMedia]} /> */}


                                {!random ?
                                    // @ts-ignore: Unreachable code error
                                    mediaLinks[numberMedia].slice(3, 7) != "SHOW" ?
                                        // @ts-ignore: Unreachable code error
                                        <p className="has-text-centered responseBlindtest">{mediaLinks[numberMedia].slice(3, -4)}</p>
                                        :
                                        // @ts-ignore: Unreachable code error
                                        <p className="has-text-centered responseBlindtest">{mediaLinks[numberMedia].slice(8, -4)}</p>
                                    :
                                    // @ts-ignore: Unreachable code error
                                    slinceRandomMediaShow() != "SHOW" ?
                                        // @ts-ignore: Unreachable code error
                                        <p className="has-text-centered responseBlindtest">{sliceRandomMedia()}</p>
                                        :
                                        // @ts-ignore: Unreachable code error
                                        <p className="has-text-centered responseBlindtest">{sliceWithoutShow()}</p>
                                }

                            </div>
                            :
                            <img src="../img/logo.png" alt="img" className="logoBlindtest" />}
                    </div>
                }
            </>}
            {numberMedia + 1 > mediaLinks.length && !random && <>
                <figure className="image is-fullwidth">
                    <img src="../img/over.png" alt="img" />
                </figure>
                <div className="columns mt-5">
                    <div className="column is-6 is-offset-3 has-text-centered">
                        {/* @ts-ignore: Unreachable code error */}
                        <ReseauxSociauxCadre />
                    </div>
                </div>
            </>}

        </div>
    )
}

export default BlindTest