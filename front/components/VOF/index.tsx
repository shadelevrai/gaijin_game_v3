import { FunctionComponent, useEffect, useState } from "react";
import socketIOClient from "socket.io-client";
import { useRouter } from "next/router"
import queryString from "query-string";
import { getGameJSONAPI } from "../../libs/API";
import ReseauxSociaux from "../ReseauxSociaux";

const socket = socketIOClient("http://localhost:8888");

interface Props {
    gameNumber: string
}


const VOF: FunctionComponent<Props> = ({ gameNumber }) => {

    const [launchGame, setLaunchGame] = useState<boolean>(false)
    const [numberMedia, setNumberMedia] = useState<number>(0)
    const [questions, setQuestions] = useState<[]>([]);
    const [showResponse, setShowResponse] = useState<boolean>(false);

    const router = useRouter()
    const { game } = queryString.parse(location.search);
    console.log("🚀 ~ file: index.tsx ~ line 23 ~ game", game)

    useEffect(() => {
        getGame()
        socket.on("change-media", numberMedia => {
            setNumberMedia(numberMedia)
            setShowResponse(false)
        })
        socket.on("show-response", () => {
            setShowResponse(true)
        })
        socket.on("launchGame", () => {
            setLaunchGame(true)
        })
        socket.on("return-menu", () => {
            router.reload()
        })
    }, [])

    async function getGame() {
        const data = {
            jeu: await getGameJSONAPI("VOF", gameNumber, "localhost")
        }
        const realArray = [
            {
                question: null,
                response: null,
            },
            {
                question: null,
                response: null,
            },
            {
                question: null,
                response: null,
            },
            {
                question: null,
                response: null,
            },
            {
                question: null,
                response: null,
            },
            {
                question: null,
                response: null,
            },
            {
                question: null,
                response: null,
            },
            {
                question: null,
                response: null,
            },
            {
                question: null,
                response: null,
            },
            {
                question: null,
                response: null,
            },
            {
                question: null,
                response: null,
            },
            {
                question: null,
                response: null,
            },
            {
                question: null,
                response: null,
            },
            {
                question: null,
                response: null,
            },
            {
                question: null,
                response: null,
            },
            {
                question: null,
                response: null,
            },
            {
                question: null,
                response: null,
            },
            {
                question: null,
                response: null,
            },
            {
                question: null,
                response: null,
            },
            {
                question: null,
                response: null,
            },
        ];
        for (const [key, value] of Object.entries(data.jeu)) {
            if (key === "question1") {
                {/* @ts-ignore */}
                realArray[0].question = value;
            }
            if (key === "reponse1") {
                {/* @ts-ignore */}
                realArray[0].response = value;
            }
            if (key === "question2") {
                {/* @ts-ignore */}
                realArray[1].question = value;
            }
            if (key === "reponse2") {
                {/* @ts-ignore */}
                realArray[1].response = value;
            }
            if (key === "question3") {
                {/* @ts-ignore */}
                realArray[2].question = value;
            }
            if (key === "reponse3") {
                {/* @ts-ignore */}
                realArray[2].response = value;
            }
            if (key === "question4") {
                {/* @ts-ignore */}
                realArray[3].question = value;
            }
            if (key === "reponse4") {
                {/* @ts-ignore */}
                realArray[3].response = value;
            }
            if (key === "question5") {
                {/* @ts-ignore */}
                realArray[4].question = value;
            }
            if (key === "reponse5") {
                {/* @ts-ignore */}
                realArray[4].response = value;
            }
            if (key === "question6") {
                {/* @ts-ignore */}
                realArray[5].question = value;
            }
            if (key === "reponse6") {
                {/* @ts-ignore */}
                realArray[5].response = value;
            }
            if (key === "question7") {
                {/* @ts-ignore */}
                realArray[6].question = value;
            }
            if (key === "reponse7") {
                {/* @ts-ignore */}
                realArray[6].response = value;
            }
            if (key === "question8") {
                {/* @ts-ignore */}
                realArray[7].question = value;
            }
            if (key === "reponse8") {
                {/* @ts-ignore */}
                realArray[7].response = value;
            }
            if (key === "question9") {
                {/* @ts-ignore */}
                realArray[8].question = value;
            }
            if (key === "reponse9") {
                {/* @ts-ignore */}
                realArray[8].response = value;
            }
            if (key === "question10") {
                {/* @ts-ignore */}
                realArray[9].question = value;
            }
            if (key === "reponse10") {
                {/* @ts-ignore */}
                realArray[9].response = value;
            }
            if (key === "question11") {
                {/* @ts-ignore */}
                realArray[10].question = value;
            }
            if (key === "reponse11") {
                {/* @ts-ignore */}
                realArray[10].response = value;
            }
            if (key === "question12") {
                {/* @ts-ignore */}
                realArray[11].question = value;
            }
            if (key === "reponse12") {
                {/* @ts-ignore */}
                realArray[11].response = value;
            }
            if (key === "question13") {
                {/* @ts-ignore */}
                realArray[12].question = value;
            }
            if (key === "reponse13") {
                {/* @ts-ignore */}
                realArray[12].response = value;
            }
            if (key === "question14") {
                {/* @ts-ignore */}
                realArray[13].question = value;
            }
            if (key === "reponse14") {
                {/* @ts-ignore */}
                realArray[13].response = value;
            }
            if (key === "question15") {
                {/* @ts-ignore */}
                realArray[14].question = value;
            }
            if (key === "reponse15") {
                {/* @ts-ignore */}
                realArray[14].response = value;
            }
            if (key === "question16") {
                {/* @ts-ignore */}
                realArray[15].question = value;
            }
            if (key === "reponse16") {
                {/* @ts-ignore */}
                realArray[15].response = value;
            }
            if (key === "question17") {
                {/* @ts-ignore */}
                realArray[16].question = value;
            }
            if (key === "reponse17") {
                {/* @ts-ignore */}
                realArray[16].response = value;
            }
            if (key === "question18") {
                {/* @ts-ignore */}
                realArray[17].question = value;
            }
            if (key === "reponse18") {
                {/* @ts-ignore */}
                realArray[17].response = value;
            }
            if (key === "question19") {
                {/* @ts-ignore */}
                realArray[18].question = value;
            }
            if (key === "reponse19") {
                {/* @ts-ignore */}
                realArray[18].response = value;
            }
            if (key === "question20") {
                {/* @ts-ignore */}
                realArray[19].question = value;
            }
            if (key === "reponse20") {
                {/* @ts-ignore */}
                realArray[19].response = value;
            }
        }
        {/* @ts-ignore */}
        setQuestions(realArray);

    }

    return (
        <div>
            <img src="../img/backVOF.png" className="backgroundvof" />
            {!launchGame && <>
                <div>
                    <p className="titlevof">Vrai ou faux</p>
                    <img className="logovof" src="../img/logo.png" alt="img" />
                </div>

            </>}
            {launchGame && questions.length !== numberMedia && <>
                <p className="numbervof">
                    {numberMedia + 1} / {questions.length}
                </p>
                <p className="titlevof2">Vrai ou faux</p>
                {/*  @ts-ignore */}
                <p className="questionvof"> {questions[numberMedia].question} </p>
                {/* @ts-ignore */}
                {showResponse && <p className="responsevof"> {questions[numberMedia].response.substring(0, 4)} </p>}
                {/* @ts-ignore */}
                {showResponse && <p className="responsevof2"> {questions[numberMedia].response.slice(5)} </p>}
                <img className="logovof2" src="../img/logo.png" alt="img" />
                <div style={{ "position": "absolute", "marginTop": "90vh", "marginLeft": "75vw" }}>
                    <ReseauxSociaux />
                </div>


            </>}
            {questions.length === numberMedia && <>
                <p className="thank">Merci d'avoir joué</p>
            </>}
        </div>
    )
}

export default VOF