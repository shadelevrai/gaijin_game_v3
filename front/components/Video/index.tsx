import { FunctionComponent, useEffect, useState } from "react";
import socketIOClient from "socket.io-client";
import { useRouter } from "next/router"
import ReactPlayer from "react-player/lazy";
const socket = socketIOClient("http://localhost:8888");

interface Props {
    gameNumber: string
}

const Karaoke: FunctionComponent<Props> = ({gameNumber }) => {
    const router = useRouter()

    const [mediaLinks, getMediaLinks] = useState<[]>([])
    const [indexMedia, setIndexMedia] = useState<number>(0)
    const [volume, setVolume] = useState<number>(0.5);
    const [message, setMessage] = useState<string>("")

    useEffect(() => {
        // getGame()
        socket.on("karaoke-playlist", (mediaChoice) => getMediaLinks(mediaChoice))
        socket.on("return-menu", () => {
            router.reload()
        })
        socket.on("message_for_video",(message)=>setMessage(message))
    }, [])

    useEffect(() => {
        socket.on("volume-up", () => {
            volume < 0.9 && setVolume(volume + 0.1)
        })
        socket.on("volume-down", () => {
            volume > 0.1 && setVolume(volume - 0.1)
        })
    }, [volume])


    return (
        <div>{mediaLinks.length > 0 && mediaLinks.length != indexMedia ? <>
            <ReactPlayer
                url={`/media/video/${gameNumber}/${mediaLinks[indexMedia]}`}
                width="100vw"
                height="100vh"
                controls={false}
                playing={true}
                onReady={() => {
                    setVolume(0.5)
                    return true
                }}
                onEnded={() => setIndexMedia(indexMedia + 1)}
                volume={volume} style={{position:"absolute"}} />
            <figure className="image" style={{ position: "absolute", right: "5vw", width: "15vw", top: "5vh" }}>
                <img src="../img/logo.png" style={{position:"absolute"}}/>

            </figure>
                <p style={{position:"absolute",marginLeft:"5vw",fontSize:"3vw",marginTop:"84vh",color:"white"}} className="has-text-weight-bold message_for_video"> {message} </p>
        </>
            :
            <>
                <img src="../img/logo.png" style={{ width: "50vw", marginLeft: "25vw", marginTop: "10vh" }} />
                <div /*style={{ marginLeft: "25vw" }}*/>
                    {gameNumber === "Karaoke" && <p className="has-text-centered is-size-1 has-text-weight-bold is-uppercase">C'est l'heure du karaoke !</p>}

                </div>
            </>
        }

        </div>
    )
}

export default Karaoke