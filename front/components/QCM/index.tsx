import { FunctionComponent, useEffect, useState } from "react";
import socketIOClient from "socket.io-client";
import { useRouter } from "next/router"
import { getGameJSONAPI } from "../../libs/API";
import { convertQCMObject } from "../../libs/convert"
import ReseauxSociauxCadre from "../ReseauxSociauxCadre";

const socket = socketIOClient("http://localhost:8888");

interface Props {
    gameNumber: string
}

const QCM: FunctionComponent<Props> = ({ gameNumber }) => {

    const router = useRouter()

    const [launchGame, setLaunchGame] = useState<boolean>(false)
    const [questions, setQuestions] = useState<[string] | []>([])
    const [showResponse, setShowResponse] = useState(false);
    const [numberMedia, setNumberMedia] = useState<number>(0)


    useEffect(() => {
        getGame()
        socket.on("launchGame", () => {
            setLaunchGame(true)
        })
        socket.on("return-menu", () => {
            router.reload()
        })
        socket.on("change-media", numberMedia => {
            setNumberMedia(numberMedia)
            setShowResponse(false)
        })
        socket.on("show-response", () => {
            setShowResponse(true)
        })
    }, [])

    async function getGame(): Promise<void> {
        const data = {
            jeu: await getGameJSONAPI("QCM", gameNumber, "localhost")
        }
        console.log(data)
        const realArray = convertQCMObject(data)
        {/* @ts-ignore: Unreachable code error */}
        setQuestions(realArray);
    }


    return (<div>
        <img src="../img/back.jpg" alt="imagefondqcm" className="imgbackgroundqcm" />
        {!launchGame && <>
            <h1 className="titleqcm">Questionnaire</h1>
            <img className="logojdm" src="../img/logo.png" alt="img" />

        </>}
        {launchGame && questions.length !== numberMedia && <>
            {!showResponse && (
                <div>
                    {/* @ts-ignore: Unreachable code error */}
                    <p className="questionqcm">{questions[numberMedia].question}</p>
                    {/* @ts-ignore: Unreachable code error */}
                    <p className="proposition1qcm"> {questions[numberMedia].response[0].proposal} </p>
                    {/* @ts-ignore: Unreachable code error */}
                    <p className="proposition2qcm"> {questions[numberMedia].response[1].proposal} </p>
                    {/* @ts-ignore: Unreachable code error */}
                    <p className="proposition3qcm"> {questions[numberMedia].response[2].proposal} </p>
                    {/* @ts-ignore: Unreachable code error */}
                    <p className="proposition4qcm"> {questions[numberMedia].response[3].proposal} </p>
                    <img className="logoqcm" src="../img/logo.png" alt="img" />
                    <p className="numberqcm">
                        {numberMedia+1} / {questions.length}
                    </p>
                </div>
            )}
            {showResponse && (
                <div>
                    {/* @ts-ignore: Unreachable code error */}
                    <p className="questionqcm">{questions[numberMedia].question}</p>
                    {/* @ts-ignore: Unreachable code error */}
                    {questions[numberMedia].response[0].ok && <p className="proposition1qcm"> {questions[numberMedia].response[0].proposal}
                    {/* @ts-ignore: Unreachable code error */} </p>}
                    {questions[numberMedia].response[1].ok && <p className="proposition2qcm"> {questions[numberMedia].response[1].proposal}
                    {/* @ts-ignore: Unreachable code error */} </p>}
                    {questions[numberMedia].response[2].ok && <p className="proposition3qcm"> {questions[numberMedia].response[2].proposal}
                    {/* @ts-ignore: Unreachable code error */} </p>}
                    {questions[numberMedia].response[3].ok && <p className="proposition4qcm"> {questions[numberMedia].response[3].proposal} </p>}
                    <img className="logoqcm" src="../img/logo.png" alt="img" />
                    <p className="numberqcm">
                        {numberMedia+1} / {questions.length}
                    </p>
                </div>
            )}
        </>}
        {launchGame && questions.length === numberMedia && <>
                <div className="columns mt-5" style={{position:"absolute"}}>
                <p className="thanksjdm">Merci d'avoir participé</p>
                  
                </div>
        </>}
        <>



        </>
    </div>)

}

export default QCM